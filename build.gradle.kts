plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

repositories {
    jcenter()
}

val javaFXModules = listOf("base", "controls", "fxml", "swing", "graphics")
val supportedPlatforms = listOf("linux", "mac", "win")

dependencies {
    // Dependencies
    implementation("com.google.guava:guava:29.0-jre")
    for(platform in supportedPlatforms) {
        for(module in javaFXModules) {
            implementation("org.openjfx:javafx-$module:14.0.2.1:$platform")
        }
    }
    implementation("mysql:mysql-connector-java:8.0.20")
    implementation("commons-codec:commons-codec:1.11")

    // Use JUnit Jupiter.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
}

application {
    mainClassName = "it.unibo.itsupportmanager.application.Launcher"
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform()
}
