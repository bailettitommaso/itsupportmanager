/*TRUNCATE TABLE users;
INSERT INTO users (id, email, name, surname, password, role)
VALUES (1, 'user_1_email@domain.com', 'name_1', 'surname_1', 'password_1', 1);*/

DELETE FROM companies;
INSERT INTO companies (ref, name, user_id)
VALUES ('company_ref_1', 'company_1', 1);
INSERT INTO companies (id, ref, name, user_id)
VALUES ('company_ref_2', 'company_2', 2);
INSERT INTO companies (id, ref, name, user_id)
VALUES ('company_ref_3', 'company_3', 3);

DELETE FROM pack;
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_1', 1, 10, 1);
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_2', 2, 20, 1);
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_3', 3, 30, 1);
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_4', 3, 30, 1);
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_5', 8, 80, 2);
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_6', 10, 100, 2);
INSERT INTO pack (description, time, price, company_id)
VALUES ('pack_description_7', 16, 160, 3);

DELETE FROM commissions;
INSERT INTO commissions (title, priority, status, source)
VALUES ('commission_1', 'medium', 'active', 'commission_source_1');

INSERT INTO assistances (description, notes, commission_id)
VALUES ('Descrizione assistenza 1', 'Note assistenza 1', 28);