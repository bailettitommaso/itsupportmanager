-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`roles`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
TRUNCATE `itsupportmanager`.`roles`;
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (1, 'SUPERADMIN');
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (2, 'ADMIN');
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (3, 'EMPLOYEE');
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (4, 'GUEST');

COMMIT;