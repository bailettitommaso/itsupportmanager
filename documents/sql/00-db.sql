-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema itsupportmanager
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema itsupportmanager
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `itsupportmanager`;
USE `itsupportmanager`;

-- -----------------------------------------------------
-- Table `itsupportmanager`.`commissions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`commissions`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`commissions`
(
    `id`           INT                                   NOT NULL AUTO_INCREMENT,
    `title`        VARCHAR(100)                          NOT NULL,
    `description`  TEXT                                  NULL DEFAULT NULL,
    `priority`     ENUM ('low', 'medium', 'high')        NOT NULL,
    `status`       ENUM ('queue', 'active', 'completed') NOT NULL,
    `source`       VARCHAR(50)                           NULL DEFAULT NULL,
    `worked_hours` INT                                   NULL DEFAULT '0',
    `company_id`   INT                                   NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`assistances`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`assistances`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`assistances`
(
    `id`            INT          NOT NULL AUTO_INCREMENT,
    `description`   VARCHAR(255) NOT NULL,
    `notes`         TEXT         NULL     DEFAULT NULL,
    `commission_id` INT          NOT NULL,
    `created_on`    DATETIME     NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`id`, `commission_id`),
    INDEX `fk_assistance_commissions1_idx` (`commission_id` ASC) VISIBLE,
    CONSTRAINT `fk_assistance_commissions1`
        FOREIGN KEY (`commission_id`)
            REFERENCES `itsupportmanager`.`commissions` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`roles`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`roles`
(
    `id`   INT         NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `nome_UNIQUE` (`name` ASC) VISIBLE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`users`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`users`
(
    `id`       INT          NOT NULL AUTO_INCREMENT,
    `email`    VARCHAR(45)  NOT NULL,
    `name`     VARCHAR(45)  NULL DEFAULT NULL,
    `surname`  VARCHAR(45)  NULL DEFAULT NULL,
    `password` VARCHAR(255) NOT NULL,
    `role_id`  INT          NOT NULL,
    PRIMARY KEY (`id`, `role_id`),
    UNIQUE INDEX `username_UNIQUE` (`email` ASC) VISIBLE,
    INDEX `fk_users_roles_idx` (`role_id` ASC) VISIBLE,
    CONSTRAINT `fk_users_roles`
        FOREIGN KEY (`role_id`)
            REFERENCES `itsupportmanager`.`roles` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`companies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`companies`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`companies`
(
    `id`      INT          NOT NULL AUTO_INCREMENT,
    `ref`     VARCHAR(20)  NOT NULL,
    `name`    VARCHAR(255) NOT NULL,
    `user_id` INT          NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `ref_UNIQUE` (`ref` ASC) VISIBLE,
    INDEX `fk_companies_users1_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_companies_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `itsupportmanager`.`users` (`id`)
            ON DELETE RESTRICT
            ON UPDATE RESTRICT
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`orders`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`orders`
(
    `id`                     INT                                                  NOT NULL AUTO_INCREMENT,
    `description`            VARCHAR(255)                                         NULL,
    `status`                 ENUM ('created', 'ordered', 'received', 'delivered') NOT NULL,
    `supplier`               VARCHAR(255)                                         NOT NULL,
    `expected_delivery_date` DATE                                                 NULL     DEFAULT NULL,
    `commission_id`          INT                                                  NOT NULL,
    `user_id`                INT                                                  NOT NULL,
    `created_on`             DATETIME                                             NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`id`, `commission_id`, `user_id`),
    INDEX `fk_orders_commissions1_idx` (`commission_id` ASC) VISIBLE,
    INDEX `fk_orders_users1_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_orders_commissions1`
        FOREIGN KEY (`commission_id`)
            REFERENCES `itsupportmanager`.`commissions` (`id`),
    CONSTRAINT `fk_orders_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `itsupportmanager`.`users` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`products`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`products`
(
    `id`          INT         NOT NULL AUTO_INCREMENT,
    `ref`         VARCHAR(45) NOT NULL,
    `description` TEXT        NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`orders_products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`orders_products`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`orders_products`
(
    `order_id`   INT            NOT NULL,
    `product_id` INT            NOT NULL,
    `quantity`   INT UNSIGNED   NULL DEFAULT '1',
    `price`      DECIMAL(10, 2) NOT NULL,
    PRIMARY KEY (`order_id`, `product_id`),
    INDEX `fk_orders_has_product_product1_idx` (`product_id` ASC) VISIBLE,
    INDEX `fk_orders_has_product_orders1_idx` (`order_id` ASC) VISIBLE,
    CONSTRAINT `fk_orders_has_product_orders1`
        FOREIGN KEY (`order_id`)
            REFERENCES `itsupportmanager`.`orders` (`id`),
    CONSTRAINT `fk_orders_has_product_product1`
        FOREIGN KEY (`product_id`)
            REFERENCES `itsupportmanager`.`products` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`pack`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`pack`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`pack`
(
    `id`          INT            NOT NULL AUTO_INCREMENT,
    `description` VARCHAR(255)   NULL DEFAULT NULL,
    `time`        INT            NOT NULL,
    `price`       DECIMAL(10, 2) NOT NULL,
    `company_id`  INT            NOT NULL,
    PRIMARY KEY (`id`, `company_id`),
    INDEX `fk_pack_companies1_idx` (`company_id` ASC) VISIBLE,
    CONSTRAINT `fk_pack_companies1`
        FOREIGN KEY (`company_id`)
            REFERENCES `itsupportmanager`.`companies` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`quotes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`quotes`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`quotes`
(
    `id`            INT                                                      NOT NULL AUTO_INCREMENT,
    `description`   VARCHAR(255)                                             NOT NULL,
    `status`        ENUM ('proposed', 'to evaluate', 'approved', 'rejected') NOT NULL,
    `price`         DECIMAL(10, 2)                                           NOT NULL,
    `commission_id` INT                                                      NOT NULL,
    `user_id`       INT                                                      NOT NULL,
    `created_on`    DATETIME                                                 NOT NULL DEFAULT NOW(),
    PRIMARY KEY (`id`, `commission_id`, `user_id`),
    INDEX `fk_quotes_commission1_idx` (`commission_id` ASC) VISIBLE,
    INDEX `fk_quotes_users1_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_quotes_commission1`
        FOREIGN KEY (`commission_id`)
            REFERENCES `itsupportmanager`.`commissions` (`id`),
    CONSTRAINT `fk_quotes_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `itsupportmanager`.`users` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`users_assistances`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `itsupportmanager`.`users_assistances`;

CREATE TABLE IF NOT EXISTS `itsupportmanager`.`users_assistances`
(
    `user_id`       INT NOT NULL,
    `assistance_id` INT NOT NULL,
    PRIMARY KEY (`user_id`, `assistance_id`),
    INDEX `fk_users_has_assistance_assistance1_idx` (`assistance_id` ASC) VISIBLE,
    INDEX `fk_users_has_assistance_users1_idx` (`user_id` ASC) VISIBLE,
    CONSTRAINT `fk_users_has_assistance_assistance1`
        FOREIGN KEY (`assistance_id`)
            REFERENCES `itsupportmanager`.`assistances` (`id`),
    CONSTRAINT `fk_users_has_assistance_users1`
        FOREIGN KEY (`user_id`)
            REFERENCES `itsupportmanager`.`users` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
