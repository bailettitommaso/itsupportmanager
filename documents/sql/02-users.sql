-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
TRUNCATE `itsupportmanager`.`users`;
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`)
VALUES (1, 'superuser@example.com', 'superuser', 'superuser',
        '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 1);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`)
VALUES (2, 'admin@example.com', 'admin', 'admin', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
        2);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`)
VALUES (3, 'employee@example.com', 'employee', 'employee',
        '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`)
VALUES (4, 'guest@example.com', 'guest', 'guest', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
        4);

COMMIT;