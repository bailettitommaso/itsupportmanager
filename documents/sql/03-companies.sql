-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`companies`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`)
VALUES (1, 'superuser@reference', 'superuserCompany', 1);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`)
VALUES (2, 'admin@reference', 'adminCompany', 2);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`)
VALUES (3, 'employee@reference', 'employeeCompany', 3);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`)
VALUES (4, 'guest@reference', 'guestCompany', 4);

COMMIT;