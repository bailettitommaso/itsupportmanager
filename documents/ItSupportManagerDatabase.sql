-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema itsupportmanager
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema itsupportmanager
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `itsupportmanager` ;
USE `itsupportmanager` ;

-- -----------------------------------------------------
-- Table `itsupportmanager`.`commissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`commissions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `priority` ENUM('low', 'medium', 'high') NOT NULL,
  `status` ENUM('queue', 'active', 'completed') NOT NULL,
  `source` ENUM('email', 'telephone', 'fax', 'ticket', 'intern', 'meeting') NOT NULL,
  `company_id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`assistances`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`assistances` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `notes` TEXT NULL DEFAULT NULL,
  `status` ENUM('active', 'completed') NOT NULL DEFAULT 'active',
  `worked_hours` INT NOT NULL DEFAULT 0,
  `commission_id` INT NOT NULL,
  `created_on` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`, `commission_id`),
  INDEX `fk_assistance_commissions1_idx` (`commission_id` ASC) VISIBLE,
  CONSTRAINT `fk_assistance_commissions1`
    FOREIGN KEY (`commission_id`)
    REFERENCES `itsupportmanager`.`commissions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nome_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `surname` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`id`, `role_id`),
  UNIQUE INDEX `username_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_users_roles_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`role_id`)
    REFERENCES `itsupportmanager`.`roles` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`companies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ref` VARCHAR(64) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `ref_UNIQUE` (`ref` ASC) VISIBLE,
  INDEX `fk_companies_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_companies_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `itsupportmanager`.`users` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `status` ENUM('created', 'ordered', 'received', 'delivered') NOT NULL DEFAULT 'created',
  `supplier` VARCHAR(255) NOT NULL,
  `expected_delivery_date` DATE NULL DEFAULT NULL,
  `commission_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `created_on` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`, `commission_id`, `user_id`),
  INDEX `fk_orders_commissions1_idx` (`commission_id` ASC) VISIBLE,
  INDEX `fk_orders_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_orders_commissions1`
    FOREIGN KEY (`commission_id`)
    REFERENCES `itsupportmanager`.`commissions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `itsupportmanager`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ref` VARCHAR(45) NOT NULL,
  `description` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `ref_UNIQUE` (`ref` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`orders_products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`orders_products` (
  `order_id` INT NOT NULL,
  `product_id` INT NOT NULL,
  `quantity` INT UNSIGNED NULL DEFAULT '1',
  `price` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`order_id`, `product_id`),
  INDEX `fk_orders_has_product_product1_idx` (`product_id` ASC) VISIBLE,
  INDEX `fk_orders_has_product_orders1_idx` (`order_id` ASC) VISIBLE,
  CONSTRAINT `fk_orders_has_product_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `itsupportmanager`.`orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_has_product_product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `itsupportmanager`.`products` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`pack`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`pack` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `time` INT NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `company_id` INT NOT NULL,
  PRIMARY KEY (`id`, `company_id`),
  INDEX `fk_pack_companies1_idx` (`company_id` ASC) VISIBLE,
  CONSTRAINT `fk_pack_companies1`
    FOREIGN KEY (`company_id`)
    REFERENCES `itsupportmanager`.`companies` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`quotes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`quotes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `status` ENUM('proposed', 'revisioning', 'approved', 'rejected') NOT NULL DEFAULT 'proposed',
  `price` DECIMAL(10,2) NOT NULL,
  `commission_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `created_on` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`, `commission_id`, `user_id`),
  INDEX `fk_quotes_commission1_idx` (`commission_id` ASC) VISIBLE,
  INDEX `fk_quotes_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_quotes_commission1`
    FOREIGN KEY (`commission_id`)
    REFERENCES `itsupportmanager`.`commissions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_quotes_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `itsupportmanager`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `itsupportmanager`.`users_assistances`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `itsupportmanager`.`users_assistances` (
  `user_id` INT NOT NULL,
  `assistance_id` INT NOT NULL,
  PRIMARY KEY (`user_id`, `assistance_id`),
  INDEX `fk_users_has_assistance_assistance1_idx` (`assistance_id` ASC) VISIBLE,
  INDEX `fk_users_has_assistance_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_assistance_assistance1`
    FOREIGN KEY (`assistance_id`)
    REFERENCES `itsupportmanager`.`assistances` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_assistance_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `itsupportmanager`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`commissions`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (1, 'Installazione sistemi', 'Installazione di postazioni computer', 'low', 'completed', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (2, 'Manutenzione sistemi informatici', 'Manutenzione dei sistemi informatici installati in precendenza', 'high', 'completed', 'telephone', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (3, 'Installazione e collaudo rete locale', 'Installazione di modem e switch ethernet, cablaggio e messa a punto', 'medium', 'completed', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (4, 'Riparazione computer', 'Riparazione hardware computer', 'high', 'completed', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (5, 'Manutenzione stampanti', 'Cambio toner per stampanti aziendali e controllo', 'low', 'queue', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (6, 'Miglioramento wireless', 'Miglioramento della rete locale wifi con upgrade dei ripetitori', 'medium', 'active', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (7, 'Manutenzione rete locale', 'Manutenzione della rete locale aziendale a seguito di vari problemi riscontrati', 'medium', 'active', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (8, 'Sostituzione hardware difettoso', 'Sostituzione componente difettoso e test del sistema', 'low', 'completed', 'email', 1);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (9, 'Installazione sistemi', 'Installazione di postazioni computer', 'high', 'completed', 'email', 2);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (10, 'Installazione e collaudo rete locale', 'Installazione di modem e switch ethernet, cablaggio e messa a punto', 'medium', 'completed', 'telephone', 2);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (11, 'Manutenzione stampanti', 'Cambio toner per stampanti aziendali e controllo', 'low', 'queue', 'email', 2);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (12, 'Installazione sistemi', 'Installazione di postazioni computer', 'medium', 'completed', 'email', 3);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (13, 'Riparazione computer', 'Riparazione hardware computer', 'high', 'completed', 'email', 3);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (14, 'Miglioramento wireless', 'Miglioramento della rete locale wifi con upgrade dei ripetitori', 'medium', 'active', 'email', 3);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (15, 'Riparazione computer', 'Riparazione hardware computer', 'low', 'queue', 'telephone', 3);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (16, 'Installazione e collaudo rete locale', 'Installazione di modem e switch ethernet, cablaggio e messa a punto', 'medium', 'completed', 'email', 13);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (17, 'Manutenzione stampanti', 'Cambio toner per stampanti aziendali e controllo', 'low', 'queue', 'email', 13);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (18, 'Installazione sistemi', 'Installazione di postazioni computer', 'low', 'completed', 'email', 9);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (19, 'Sostituzione hardware difettoso', 'Sostituzione componente difettoso e test del sistema', 'low', 'active', 'telephone', 9);
INSERT INTO `itsupportmanager`.`commissions` (`id`, `title`, `description`, `priority`, `status`, `source`, `company_id`) VALUES (20, 'Installazione sistemi', 'Installazione di postazioni computer', 'high', 'completed', 'telephone', 9);

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`assistances`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (1, 'Installazione e collaudo', 'Installate anche multiprese', 'completed', 5, 1, '2020-01-20');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (2, 'Manutenzione', NULL, 'completed', 5, 2, '2019-03-09');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (3, 'Manutenzione', NULL, 'completed', 5, 2, '2018-11-01');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (4, 'Installazione e collaudo', NULL, 'completed', 6, 3, '2020-02-16');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (5, 'Riparazione', NULL, 'completed', 2, 4, '2018-04-12');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (6, 'Assistenza tecnica', 'Aggiungere ripetitori esterni per migliorare la copertura', 'active', 3, 6, '2020-05-23');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (7, 'Riparazione', NULL, 'completed', 2, 8, '2020-05-24');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (8, 'Installazione e collaudo', NULL, 'completed', 9, 9, '2017-12-10');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (9, 'Installazione e collaudo', NULL, 'completed', 4, 10, '2019-02-05');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (10, 'Installazione e collaudo', NULL, 'completed', 11, 12, '2019-01-07');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (11, 'Riparazione', NULL, 'completed', 3, 13, '2020-01-25');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (12, 'Assistenza tecnica', 'Nelle aree piu\' difficili installare powerline con wifi', 'active', 4, 14, '2020-07-15');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (13, 'Installazione e collaudo', NULL, 'completed', 5, 16, '2019-10-13');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (14, 'Installazione e collaudo', NULL, 'completed', 5, 18, '2018-06-19');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (15, 'Riparazione', 'Modulo RAM difettoso', 'active', 2, 19, '2020-07-01');
INSERT INTO `itsupportmanager`.`assistances` (`id`, `description`, `notes`, `status`, `worked_hours`, `commission_id`, `created_on`) VALUES (16, 'Installazione e collaudo', NULL, 'completed', 5, 20, '2019-09-27');

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`roles`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (1, 'SUPERADMIN');
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (2, 'ADMIN');
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (3, 'EMPLOYEE');
INSERT INTO `itsupportmanager`.`roles` (`id`, `name`) VALUES (4, 'GUEST');

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (1, 'superuser@itsm.com', 'superuser', 'superuser', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 1);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (2, 'admin@itsm.com', 'admin', 'admin', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 2);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (3, 'employee@itsm.com', 'employee', 'employee', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (4, 'guest@itsm.com', 'guest', 'guest', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (5, 'riccardo.omiccioli@itsm.it', 'Riccardo', 'Omiccioli', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 2);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (6, 'tommaso.bailetti@itsm.it', 'Tommaso', 'Bailetti', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 2);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (7, 'info@computeritalia.it', 'Luigi', 'Lombardi', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (8, 'marco.longo@gruppoitd.it', 'Marco', 'Longo', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (9, 'francesco.bellini@techsystem.it', 'Francesco', 'Bellini', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (10, 'maria.franco@cbd.it', 'Maria', 'Franco', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (11, 'john.doe@hp.com', 'John', 'Doe', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (12, 'vendite@asystem.it', 'ASystem', NULL, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (13, 'frank.mueller@siemens.de', 'Frank', 'Mueller', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (14, 'michael.schulz@siemens.de', 'Michael', 'Schulz', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (15, 'vittoria.morelli@ibm.it', 'Vittoria', 'Morelli', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (16, 'giovanni.bianco@rsc.it', 'Giovanni', 'Bianco', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (17, 'antonio.ruggeri@itsm.it', 'Antonio', 'Ruggeri', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (18, 'franca.orlando@itsm.it', 'Franca', 'Orlando', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (19, 'luca.molinari@itsm.it', 'Luca', 'Molinari', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (20, 'valentina.coppola@itsm.it', 'Valentina', 'Coppola', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (21, 'mario.piazza@itsm.it', 'Mario', 'Piazza', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 3);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (22, 'sonia.sartori@formulax.it', 'Sonia', 'Sartori', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (23, 'alberto.poli@tcgroup.it', 'Alberto', 'Poli', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (24, 'alessandro.barone@pcsistemi.it', 'Alessandro', 'Barone', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);
INSERT INTO `itsupportmanager`.`users` (`id`, `email`, `name`, `surname`, `password`, `role_id`) VALUES (25, 'edoardo.olivieri@datastore.it', 'Edoardo', 'Olivieri', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`companies`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (1, 'info@computeritalia.it', 'Computer Italia', 7);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (2, 'marco.longo@gruppoitd.it', 'Gruppo ITD', 8);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (3, 'francesco.bellini@techsystem.it', 'TechSystem', 9);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (4, 'maria.franco@cbd.it', 'CBD', 10);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (5, 'john.doe@hp.com', 'HP', 11);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (6, 'vendite@asystem.it', 'ASystem', 12);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (7, 'frank.mueller@siemens.de', 'Siemens', 13);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (8, 'vittoria.morelli@ibm.it', 'IBM', 15);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (9, 'giovanni.bianco@rsc.it', 'RSC', 16);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (10, 'sonia.sartori@formulax.it', 'FormulaX', 22);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (11, 'alberto.poli@tcgroup.it', 'TC Group', 23);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (12, 'alessandro.barone@pcsistemi.it', 'PC Sistemi', 24);
INSERT INTO `itsupportmanager`.`companies` (`id`, `ref`, `name`, `user_id`) VALUES (13, 'edoardo.olivieri@datastore.it', 'DataStore', 25);

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`orders`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (1, 'Ordine 5 computer', 'delivered', 'Amazon.it', '2020-01-15', 1, 19, '2020-01-14');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (2, 'Ordine modem e switch', 'delivered', 'PCTechstore.it', NULL, 3, 19, '2020-02-16');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (3, 'Stampante', 'ordered', 'Amazon.it', NULL, 5, 19, '2018-04-10');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (4, 'Ordine componenti rete wifi', 'delivered', 'BPMPower.it', NULL, 6, 19, '2020-05-21');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (5, 'Ricambio PC', 'delivered', 'PCTechstore.it', '2020-05-23', 8, 19, '2020-05-23');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (6, 'Ordine 4 computer', 'delivered', 'Amazon.it', NULL, 9, 19, '2017-12-06');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (7, 'Ordine modem, switch e cavi', 'delivered', 'BPMPower.it', NULL, 10, 19, '2019-02-03');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (8, 'Stampante', 'created', 'Amazon.it', NULL, 11, 19, '2020-01-24');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (9, 'Ordine 2 computer', 'delivered', 'PCTechstore.it', NULL, 12, 19, '2020-07-14');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (10, 'Ricambio PC', 'delivered', 'BPMPower.it', '2020-01-22', 13, 19, '2020-01-22');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (11, 'Ordine cavi ethernet', 'ordered', 'Amazon.it', NULL, 14, 19, '2018-06-19');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (12, 'Ricambio PC', 'ordered', 'PCTechstore.it', NULL, 15, 19, '2020-07-01');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (13, 'Ordine modem, switch e cavi', 'delivered', 'PCTechstore.it', '2019-09-27', 16, 19, '2019-09-27');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (14, 'Ordine 9 computer', 'delivered', 'Amazon.it', NULL, 18, 19, '2018-06-14');
INSERT INTO `itsupportmanager`.`orders` (`id`, `description`, `status`, `supplier`, `expected_delivery_date`, `commission_id`, `user_id`, `created_on`) VALUES (15, 'Ordine 2 computer', 'delivered', 'Amazon.it', NULL, 20, 19, '2019-09-25');

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`products`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (1, '690701', 'Asus Laptop S15530UF');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (2, '450002', 'Monitor Dell P2415Q');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (3, '102603', 'Monitor Dell S2240L');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (4, '847001', 'Modem TP-LINK VDSL2');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (5, '490445', 'Powerline TP-LINK AV600');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (6, '030106', 'Alimentatore TENMA 72-10');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (7, '670002', 'Multipresa elettrica Electraline 62002');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (8, '459820', 'Gruppo continuita\' APC BX700');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (9, '574801', 'Telecamera Smart Blink XT2');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (10, '293012', 'Cavo Ethernet cat 6A 10m');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (11, '204906', 'Switch NETGEAR GS305 5 porte');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (12, '888720', 'Router D-Link DWR-953');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (13, '120544', 'Router AVM FRITZ 4040');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (14, '243246', 'Cavo Ethernet cat 7A 3m');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (15, '451000', 'Cavo Ethernet cat 5E 20m');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (16, '223313', 'Cavo USB 3.0 Type A-C 2m');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (17, '239990', 'Cavo USB 3.0 Type A-A 1m');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (18, '400234', 'Hub USB 3.0 6Gbps 4 porte');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (19, '111021', 'Desktop Dell Optiflex 2400');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (20, '677124', 'Desktop HP PC M01');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (21, '009972', 'Stampante HP DeskJet 3762');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (22, '429991', 'Stampante HP OfficeJet 6950');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (23, '120018', 'Stampante Canon PIXMA TS5351');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (24, '320948', 'Monitor Samsung 27\" C27F396');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (25, '198755', 'Monitor LG 27\" 27GL850');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (26, '982345', 'Ventola Noctua NF-A14 140mm');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (27, '782455', 'Ventola Noctua NF-A6x25 60mm');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (28, '233327', 'HDD Seagate Barracuda ST2000DM 2TB');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (29, '662413', 'HDD Seagate Barracuda ST1000DM 1TB');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (30, '489102', 'HDD WesternDigital WD5000AAKX 500GB');
INSERT INTO `itsupportmanager`.`products` (`id`, `ref`, `description`) VALUES (31, '988256', 'Cavo SATA 3 6Gbps 30cm');

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`orders_products`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (1, 2, 5, 200);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (1, 19, 5, 800);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (2, 4, 2, 128.50);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (2, 11, 6, 49.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (2, 14, 12, 9.5);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (3, 21, 1, 148.70);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (4, 12, 2, 36.40);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (4, 14, 6, 11.50);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (5, 26, 2, 24.5);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (6, 20, 4, 750);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (6, 2, 4, 250);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (7, 4, 2, 122.50);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (7, 11, 4, 48.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (7, 14, 8, 12.20);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (8, 23, 1, 179.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (9, 1, 2, 890.50);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (10, 27, 1, 16.70);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (11, 14, 10, 9.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (11, 15, 10, 21.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (12, 28, 2, 54.50);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (13, 4, 2, 127.42);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (13, 5, 4, 36.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (13, 11, 4, 51.20);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (13, 10, 12, 14.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (14, 19, 9, 888.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (14, 3, 9, 221.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (15, 20, 1, 749.90);
INSERT INTO `itsupportmanager`.`orders_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES (15, 19, 1, 748.90);

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`pack`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (1, NULL, 10, 100, 1);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (2, NULL, 10, 100, 1);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (3, NULL, 5, 50, 1);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (4, NULL, 15, 150, 1);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (5, NULL, 20, 200, 1);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (6, NULL, 20, 200, 2);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (7, NULL, 20, 200, 2);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (8, NULL, 15, 150, 2);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (9, NULL, 5, 50, 3);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (10, NULL, 5, 50, 3);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (11, NULL, 5, 50, 3);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (12, NULL, 10, 100, 3);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (13, NULL, 10, 100, 13);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (14, NULL, 10, 100, 13);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (15, NULL, 10, 100, 13);
INSERT INTO `itsupportmanager`.`pack` (`id`, `description`, `time`, `price`, `company_id`) VALUES (16, NULL, 10, 100, 9);

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`quotes`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (1, 'Installazione di 7 postazioni computer complete', 'rejected', 8000, 1, 18, '2019-12-12');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (2, 'Installazione di 5 postazioni computer complete', 'approved', 5000, 1, 18, '2020-01-14');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (3, 'Riparazione hardware', 'approved', 120, 4, 18, '2018-04-10');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (4, 'Installazione nuovi ripetitori wifi', 'approved', 700, 6, 18, '2020-05-21');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (5, 'Installazione di 3 postazioni computer complete', 'rejected', 3500, 9, 18, '2017-12-01');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (6, 'Installazione di 4 postazioni computer complete', 'approved', 4000, 9, 18, '2017-12-06');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (7, 'Installazione modem e switch ethernet', 'approved', 300, 10, 18, '2019-02-03');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (8, 'Riparazione hardware', 'approved', 700, 13, 18, '2020-01-22');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (9, 'Installazione di 9 postazioni computer complete', 'approved', 10000, 18, 18, '2018-06-14');
INSERT INTO `itsupportmanager`.`quotes` (`id`, `description`, `status`, `price`, `commission_id`, `user_id`, `created_on`) VALUES (10, 'Installazione di 2 postazioni computer complete', 'approved', 1500, 20, 18, '2019-09-25');

COMMIT;


-- -----------------------------------------------------
-- Data for table `itsupportmanager`.`users_assistances`
-- -----------------------------------------------------
START TRANSACTION;
USE `itsupportmanager`;
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (5, 1);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (6, 2);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (17, 3);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (20, 4);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (21, 5);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (21, 6);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (17, 7);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (17, 8);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (20, 9);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (5, 10);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (5, 11);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (6, 12);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (6, 13);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (17, 14);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (21, 15);
INSERT INTO `itsupportmanager`.`users_assistances` (`user_id`, `assistance_id`) VALUES (20, 16);

COMMIT;

