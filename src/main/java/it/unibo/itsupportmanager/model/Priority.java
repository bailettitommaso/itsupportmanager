package it.unibo.itsupportmanager.model;

public enum Priority {

    low,

    medium,

    high;

    public static int getIntValue(Priority priority) {
        return priority.ordinal() + 1;
    }

}
