package it.unibo.itsupportmanager.model;

import java.util.Arrays;
import java.util.Optional;

public enum UserType {

    /**
     * What to say: "/gamemode creative".
     */
    SUPERUSER(1),

    /**
     * Admin Level, can do almost everything.
     */
    ADMIN(2),

    /**
     * Higher Level, can assign and do work.
     */
    EMPLOYEE(3),

    /**
     * Low Level, basic functions.
     */
    GUEST(4);

    /**
     * ID of the Database.
     */
    private final int id;

    UserType(final int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    /**
     * Allows to find a enum val from it's ID.
     *
     * @param value id of the database
     * @return since we're talking about database, it's values can change, the optional takes care of it.
     */
    public static Optional<UserType> valueOf(final int value) {
        return Arrays.stream(values())
                .filter(val -> val.id == value)
                .findFirst();
    }
}
