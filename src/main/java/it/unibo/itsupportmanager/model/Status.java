package it.unibo.itsupportmanager.model;

public enum Status {

    queue,

    active,

    completed;

    public static int getIntValue(Status status) {
        return status.ordinal() + 1;
    }

}
