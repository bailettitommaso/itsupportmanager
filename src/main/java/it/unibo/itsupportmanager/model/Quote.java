package it.unibo.itsupportmanager.model;

import java.sql.Date;

public class Quote extends Azione{

    private QuoteStatus status;
    private double price;
    private int commissionID;
    private int userID;

    public Quote(final String description, final double price, final int commissionID) {
        super(description);
        this.price = price;
        this.commissionID = commissionID;
    }

    public Quote(final String description, final double price, final int commissionID, final AzioneType type) {
        super(description, type);
        this.price = price;
        this.commissionID = commissionID;
    }

    public Quote(final String description, final QuoteStatus status, final double price, final int commissionID) {
        super(description);
        this.status = status;
        this.price = price;
        this.commissionID = commissionID;
    }

    public Quote(final int id, final String description, final QuoteStatus status, final double price, final int commissionID, final AzioneType type, final Date timestamp) {
        super(id, description, type, timestamp);
        this.status = status;
        this.price = price;
        this.commissionID = commissionID;
    }

    public Quote(final int id, final String description, final QuoteStatus status, final double price, final int commissionID, final AzioneType type, final Date timestamp, final int userID) {
        super(id, description, type, timestamp);
        this.status = status;
        this.price = price;
        this.commissionID = commissionID;
        this.userID = userID;
    }

    public QuoteStatus getStatus() {
        return status;
    }

    public void setStatus(QuoteStatus status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCommissionID() {
        return commissionID;
    }

    public void setCommissionID(int commissionID) {
        this.commissionID = commissionID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

}
