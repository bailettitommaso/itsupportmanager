package it.unibo.itsupportmanager.model;

public enum QuoteStatus {

    proposed,

    revisioning,

    approved,

    rejected;

    public static int getIntValue(OrderStatus status) {
        return status.ordinal() + 1;
    }

}
