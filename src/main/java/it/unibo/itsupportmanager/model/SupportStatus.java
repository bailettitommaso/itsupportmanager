package it.unibo.itsupportmanager.model;

public enum SupportStatus {

    active,

    completed;

    public static int getIntValue(SupportStatus status) {
        return status.ordinal() + 1;
    }

}
