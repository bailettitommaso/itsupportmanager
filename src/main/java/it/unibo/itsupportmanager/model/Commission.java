package it.unibo.itsupportmanager.model;

public class Commission {

    private int id;
    private String title;
    private String description;
    private Priority priority;
    private Status status;
    private CommissionSource source;
    private int workedHours;
    private int companyID;

    public Commission(final int id, final String title) {
        this.id = id;
        this.title = title;
    }

    public Commission(final String title, final String description, final Priority priority, final Status status, final CommissionSource source, final int companyID) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.source = source;
        this.companyID = companyID;
    }

    public Commission(final String title, final String description, final Priority priority, final Status status, final CommissionSource source) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.source = source;
    }

    public Commission(final int id, final String title, final String description, final Priority priority, final Status status, final CommissionSource source) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.source = source;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public CommissionSource getSource() {
        return source;
    }

    public void setSource(CommissionSource source) {
        this.source = source;
    }

    public int getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(int workedHours) {
        this.workedHours = workedHours;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

}
