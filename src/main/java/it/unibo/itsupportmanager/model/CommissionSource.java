package it.unibo.itsupportmanager.model;

public enum CommissionSource {

    email,

    telephone,

    fax,

    ticket,

    intern,

    meeting;

    public static int getIntValue(CommissionSource source) {
        return source.ordinal() + 1;
    }

}
