package it.unibo.itsupportmanager.model;

public class Product {

    private int id;
    private final String ref;
    private final String description;

    public Product(final String ref, final String description) {
        this.ref = ref;
        this.description = description;
    }

    public Product(final int id, final String ref, final String description) {
        this(ref, description);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getRef() {
        return ref;
    }

    public String getDescription() {
        return description;
    }
}
