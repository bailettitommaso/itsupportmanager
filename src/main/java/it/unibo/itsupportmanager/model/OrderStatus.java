package it.unibo.itsupportmanager.model;

public enum OrderStatus {

    created,

    ordered,

    received,

    delivered;

    public static int getIntValue(OrderStatus status) {
        return status.ordinal() + 1;
    }

}
