package it.unibo.itsupportmanager.model;

import java.sql.Date;

public class Azione {

    private int id;
    private String description;
    private AzioneType type;
    private Date timestamp;

    public Azione() {
        this.type = null;
        this.timestamp = null;
    }

    public Azione(String description) {
        this.description = description;
    }

    public Azione(final AzioneType type) {
        this.type = type;
    }

    public Azione(String description, AzioneType type) {
        this.description = description;
        this.type = type;
    }

    public Azione(final AzioneType type, final Date timestamp) {
        this.type = type;
        this.timestamp = timestamp;
    }

    public Azione(final int id, final String description, final AzioneType type, final Date timestamp) {
        this.id= id;
        this.description = description;
        this.type = type;
        this.timestamp = timestamp;
    }

    public Azione(String description, AzioneType type, Date timestamp) {
        this.description = description;
        this.type = type;
        this.timestamp = timestamp;
    }

    public AzioneType getType() {
        return type;
    }

    public void setType(AzioneType type) {
        this.type = type;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
