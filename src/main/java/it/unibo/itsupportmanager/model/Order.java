package it.unibo.itsupportmanager.model;

import java.sql.Date;

public class Order extends Azione {

    private OrderStatus status;
    private String supplier;
    private Date expectedDelivery;
    private int commissionID;
    private int userID;

    public Order (final String description, final String supplier, final Date expectedDelivery, final int commissionID) {
        super(description);
        this.supplier = supplier;
        this.expectedDelivery = expectedDelivery;
        this.commissionID = commissionID;
    }

    public Order (final String description, final String supplier, final Date expectedDelivery, final int commissionID, final AzioneType type) {
        super(description, type);
        this.supplier = supplier;
        this.expectedDelivery = expectedDelivery;
        this.commissionID = commissionID;
    }

    public Order (final String description, final OrderStatus status, final String supplier, final Date expectedDelivery, final int commissionID) {
        super(description);
        this.status = status;
        this.supplier = supplier;
        this.expectedDelivery = expectedDelivery;
        this.commissionID = commissionID;
    }

    public Order (final int id, final String description, final OrderStatus status, final String supplier, final Date expectedDelivery, final int commissionID, final AzioneType type, final Date timestamp) {
        super(id, description, type, timestamp);
        this.status = status;
        this.supplier = supplier;
        this.expectedDelivery = expectedDelivery;
        this.commissionID = commissionID;
    }

    public Order(final int id, final String description, final AzioneType type, final Date timestamp, final OrderStatus status, final String supplier, final Date expectedDelivery, final int commissionID, final int userID) {
        super(id, description, type, timestamp);
        this.status = status;
        this.supplier = supplier;
        this.expectedDelivery = expectedDelivery;
        this.commissionID = commissionID;
        this.userID = userID;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Date getExpectedDelivery() {
        return expectedDelivery;
    }

    public void setExpectedDelivery(Date expectedDelivery) {
        this.expectedDelivery = expectedDelivery;
    }

    public int getCommissionID() {
        return commissionID;
    }

    public void setCommissionID(int commissionID) {
        this.commissionID = commissionID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
