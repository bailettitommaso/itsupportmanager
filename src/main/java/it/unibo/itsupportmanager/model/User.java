package it.unibo.itsupportmanager.model;

import java.util.Optional;

public class User {

    private int id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private UserType userType;
    private String companyName;

    public User() {
    }

    public User(int id, String name, String surname, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public final int getId() {
        return id;
    }

    public final void setId(final int id) {
        this.id = id;
    }

    public final Optional<String> getName() {
        return Optional.ofNullable(name);
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final Optional<String> getSurname() {
        return Optional.ofNullable(surname);
    }

    public final void setSurname(final String surname) {
        this.surname = surname;
    }

    public final String getEmail() {
        return email;
    }

    public final void setEmail(final String email) {
        this.email = email;
    }

    public final String getPassword() {
        return password;
    }

    public final void setPassword(final String password) {
        this.password = password;
    }

    public final UserType getUserType() {
        return userType;
    }

    public final void setUserType(final UserType userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return this.id + " " + this.name + " " + this.surname;
    }

}
