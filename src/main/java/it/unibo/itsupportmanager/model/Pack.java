package it.unibo.itsupportmanager.model;

public class Pack {

    private int time;
    private double price;

    public Pack() {
        this.time = 0;
        this.price = 0;
    }

    public Pack(final int time, final double price) {
        this.time = time;
        this.price = price;
    }

    public int getTime() {
        return this.time;
    }

    public double getPrice() {
        return this.price;
    }

    public void setTime(final int time) {
        this.time = time;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

}
