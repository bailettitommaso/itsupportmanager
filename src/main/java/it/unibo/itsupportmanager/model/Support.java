package it.unibo.itsupportmanager.model;

import java.sql.Date;

public class Support extends Azione {

    private String notes;
    private SupportStatus status;
    private int workedHours;
    private int commissionID;

    public Support (final String description, final String notes) {
        super(description);
        this.notes = notes;
    }

    public Support (final String description, final String notes, final int commissionID) {
        super(description);
        this.notes = notes;
        this.commissionID = commissionID;
    }

    public Support (final String description, final String notes, final int commissionID, final AzioneType type) {
        super(description, type);
        this.notes = notes;
        this.commissionID = commissionID;
    }

    public Support (final String description, final String notes, final SupportStatus status, final int workedHours, final int commissionID, final AzioneType type) {
        super(description, type);
        this.notes = notes;
        this.status = status;
        this.workedHours = workedHours;
        this.commissionID = commissionID;
    }

    public Support (final String description, final String notes, final int commissionID, final AzioneType type, final Date timestamp) {
        super(description, type, timestamp);
        this.notes = notes;
        this.commissionID = commissionID;
    }

    public Support (final int id, final String description, final String notes, final int commissionID, final AzioneType type, final Date timestamp) {
        super(id, description, type, timestamp);
        this.notes = notes;
        this.commissionID = commissionID;
    }

    public Support(final int id, final String description, final AzioneType type, final Date timestamp, final String notes, final SupportStatus status, final int workedHours, final int commissionID) {
        super(id, description, type, timestamp);
        this.notes = notes;
        this.status = status;
        this.workedHours = workedHours;
        this.commissionID = commissionID;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public SupportStatus getStatus() {
        return status;
    }

    public void setStatus(SupportStatus status) {
        this.status = status;
    }

    public int getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(int workedHours) {
        this.workedHours = workedHours;
    }

    public int getCommissionID() {
        return commissionID;
    }

    public void setCommissionID(int commissionID) {
        this.commissionID = commissionID;
    }

}
