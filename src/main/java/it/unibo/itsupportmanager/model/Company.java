package it.unibo.itsupportmanager.model;

public class Company {

    private int id;
    private String ref;
    private String name;
    private int userID;

    public Company(int id, String ref, String name, int userID) {
        this.id = id;
        this.ref = ref;
        this.name = name;
        this.userID = userID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
