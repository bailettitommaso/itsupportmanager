package it.unibo.itsupportmanager.model;

public class ProductOrder extends Product {

    private int quantity;
    private double price;

    public ProductOrder(Product product, int quantity, double price) {
        super(product.getId(), product.getRef(), product.getDescription());
        this.quantity = quantity;
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
