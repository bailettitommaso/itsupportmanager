package it.unibo.itsupportmanager.model;

public enum AzioneType {

    ASSISTENZA,

    ORDINE,

    PREVENTIVO;

}
