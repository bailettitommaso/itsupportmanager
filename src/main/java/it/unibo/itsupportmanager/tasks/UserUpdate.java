package it.unibo.itsupportmanager.tasks;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class UserUpdate extends Task<Void> {

    private final User user;

    public UserUpdate(final User user) {
        this.user = user;
    }

    @Override
    protected Void call() throws Exception {
        final Connection connection = ApplicationController.getNetworkFactory().getConnection();
        final PreparedStatement statement = connection.prepareStatement("UPDATE itsupportmanager.users SET name = ?, surname = ? WHERE id = ?");
        statement.setString(1, user.getName().orElse(""));
        statement.setString(2, user.getSurname().orElse(""));
        statement.setInt(3, user.getId());
        statement.execute();
        return null;
    }
}
