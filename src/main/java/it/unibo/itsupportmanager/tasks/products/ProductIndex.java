package it.unibo.itsupportmanager.tasks.products;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.Product;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public final class ProductIndex extends Task<List<Product>> {
    @Override
    protected List<Product> call() throws Exception {
        final Connection connection = ApplicationController.getNetworkFactory().getConnection();
        final Statement statement = connection.createStatement();
        final ResultSet resultSet = statement.executeQuery("SELECT * FROM itsupportmanager.products");
        final List<Product> list = new ArrayList<>();
        while (resultSet.next()) {
            final Product product = new Product(resultSet.getInt("id"), resultSet.getString("ref"), resultSet.getString("description"));
            list.add(product);
        }
        return list;
    }
}
