package it.unibo.itsupportmanager.tasks.products;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.Product;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;

public final class ProductDelete extends Task<Void> {

    private final int idProduct;

    public ProductDelete(final Product product) {
        this.idProduct = product.getId();
    }

    @Override
    protected Void call() throws Exception {
        final Connection connection = ApplicationController.getNetworkFactory().getConnection();
        final PreparedStatement statement = connection.prepareStatement("DELETE FROM itsupportmanager.products WHERE id = ?");
        statement.setInt(1, idProduct);
        statement.execute();
        return null;
    }
}
