package it.unibo.itsupportmanager.tasks.products;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.Product;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;

public final class ProductStore extends Task<Void> {

    final private Product product;

    public ProductStore(final Product product) {
        this.product = product;
    }

    @Override
    protected Void call() throws Exception {
        final Connection connection = ApplicationController.getNetworkFactory().getConnection();
        final PreparedStatement statement = connection.prepareStatement("INSERT INTO itsupportmanager.products (ref, description) VALUES (?, ?)");
        statement.setString(1, product.getRef());
        statement.setString(2, product.getDescription());
        statement.execute();
        return null;
    }
}
