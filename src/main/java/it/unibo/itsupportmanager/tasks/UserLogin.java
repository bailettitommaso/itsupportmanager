package it.unibo.itsupportmanager.tasks;

import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.model.UserType;
import it.unibo.itsupportmanager.network.NetworkFactory;
import javafx.concurrent.Task;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserLogin extends Task<User> {

    private final NetworkFactory networkFactory;
    private final String email;
    private final String password;

    public UserLogin(final NetworkFactory networkFactory, final String email, final String password) {
        this.networkFactory = networkFactory;
        this.email = email;
        this.password = password;
    }

    @Override
    protected User call() throws Exception {
        updateMessage("Logging the user...");
        final Connection connection = networkFactory.getConnection();
        final PreparedStatement statement = connection.prepareStatement("SELECT * FROM itsupportmanager.users WHERE email = ? AND password = ?");
        statement.setString(1, email);
        statement.setString(2, DigestUtils.sha256Hex(password));
        final ResultSet resultSet = statement.executeQuery();
        final User user = new User();
        if (resultSet.next()) {
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setUserType(UserType.valueOf(resultSet.getInt("role_id")).orElseThrow());
            return user;
        } else {
            throw new Exception("User not found or wrong password.");
        }
    }

    @Override
    protected void failed() {
        updateMessage("");
        super.failed();
    }
}
