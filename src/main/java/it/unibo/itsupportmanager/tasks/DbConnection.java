package it.unibo.itsupportmanager.tasks;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.network.NetworkFactory;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public final class DbConnection extends Task<NetworkFactory> {

    private final String host;
    private final String user;
    private final String password;

    public DbConnection(final String host, final String port, final String user, final String password) {
        this.host = host + ":" + port;
        this.user = user;
        this.password = password;
    }

    @Override
    protected NetworkFactory call() throws Exception {
        NetworkFactory networkFactory = new NetworkFactory(host, user, password);
        updateMessage("Checking DB Connection...");
        final Connection connection = networkFactory.getConnection();
        final Statement statement = connection.createStatement();
        final ResultSet set = statement.executeQuery("SELECT @@version");
        updateMessage("Getting MySQL Version...");
        set.next();
        updateMessage("MySQL version found: " + set.getString(1));
        return networkFactory;
    }

    @Override
    protected void failed() {
        super.failed();
        updateMessage("");
    }
}
