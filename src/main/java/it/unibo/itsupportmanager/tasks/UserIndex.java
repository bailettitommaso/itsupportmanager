package it.unibo.itsupportmanager.tasks;

import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.model.UserType;
import it.unibo.itsupportmanager.network.NetworkFactory;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public final class UserIndex extends Task<List<User>> {

    private final NetworkFactory networkFactory;

    public UserIndex(final NetworkFactory networkFactory) {
        this.networkFactory = networkFactory;
    }

    @Override
    protected List<User> call() throws Exception {
        final List<User> userList = new ArrayList<>();
        final Connection connection = this.networkFactory.getConnection();
        final Statement statement = connection.createStatement();
        final ResultSet resultSet = statement.executeQuery("SELECT * FROM users LEFT OUTER JOIN companies c on users.id = c.user_id;");
        while (resultSet.next()) {
            final User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            user.setEmail(resultSet.getString("email"));
            user.setUserType(UserType.valueOf(resultSet.getInt("role_id")).orElseThrow());
            user.setCompanyName(resultSet.getString("c.name"));
            userList.add(user);
        }
        return userList;
    }
}
