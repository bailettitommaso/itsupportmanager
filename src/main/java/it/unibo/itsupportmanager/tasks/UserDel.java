package it.unibo.itsupportmanager.tasks;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.network.NetworkFactory;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class UserDel extends Task<Void> {

    private final int userId;

    public UserDel(final int userId) {
        this.userId = userId;
    }

    @Override
    protected Void call() throws Exception {
        final NetworkFactory networkFactory = ApplicationController.getNetworkFactory();
        final Connection connection = networkFactory.getConnection();
        final PreparedStatement statement = connection.prepareStatement("DELETE FROM itsupportmanager.users WHERE id = ?");
        statement.setInt(1, userId);
        statement.execute();
        return null;
    }
}
