package it.unibo.itsupportmanager.tasks;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.network.NetworkFactory;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class UserAdd extends Task<Void> {

    private final User user;

    public UserAdd(final User user) {
        this.user = user;
    }

    @Override
    protected Void call() throws Exception {
        final NetworkFactory factory = ApplicationController.getNetworkFactory();
        final Connection connection = factory.getConnection();
        final PreparedStatement statement = connection.prepareStatement("INSERT INTO itsupportmanager.users (email, name, surname, password, role_id) VALUES (?, ?, ?, ?, ?)");
        statement.setString(1, user.getEmail());
        statement.setString(2, user.getName().orElse(null));
        statement.setString(3, user.getSurname().orElse(null));
        statement.setString(4, user.getPassword());
        statement.setInt(5, user.getUserType().getId());
        statement.execute();
        return null;
    }
}
