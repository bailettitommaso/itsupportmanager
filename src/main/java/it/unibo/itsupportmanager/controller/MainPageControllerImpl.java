package it.unibo.itsupportmanager.controller;

import it.unibo.itsupportmanager.model.UserType;
import it.unibo.itsupportmanager.view.MainPageView;
import it.unibo.itsupportmanager.view.MainPageViewImpl;

public class MainPageControllerImpl implements MainPageController {

    private final MainPageView mainPageView;

    public MainPageControllerImpl() {
        this.mainPageView = new MainPageViewImpl(this);
    }
}