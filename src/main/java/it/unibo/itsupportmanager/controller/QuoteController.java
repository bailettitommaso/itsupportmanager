package it.unibo.itsupportmanager.controller;

import it.unibo.itsupportmanager.model.AzioneType;
import it.unibo.itsupportmanager.model.Commission;
import it.unibo.itsupportmanager.model.Quote;
import it.unibo.itsupportmanager.model.QuoteStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QuoteController {

    public static List<Quote> getAllQuotes() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.quotes q " +
                    "WHERE q.commission_id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ((Commission) ApplicationController.get("commission")).getId());
            ResultSet resultSet = statement.executeQuery();

            List<Quote> supportsList = new ArrayList<Quote>();
            while (resultSet.next()) {
                supportsList.add(new Quote(resultSet.getInt("id"), resultSet.getString("description"), QuoteStatus.valueOf(resultSet.getString("status")),
                        resultSet.getDouble("price"), resultSet.getInt("commission_id"), AzioneType.PREVENTIVO, resultSet.getDate("created_on"), resultSet.getInt("user_id")));
            }
            return supportsList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static Quote getQuote(final Quote quote) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.quotes q " +
                    "WHERE q.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, quote.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new Quote(resultSet.getInt("id"), resultSet.getString("description"), QuoteStatus.valueOf(resultSet.getString("status")),
                    resultSet.getDouble("price"), resultSet.getInt("commission_id"), AzioneType.PREVENTIVO, resultSet.getDate("created_on"), resultSet.getInt("user_id"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void addQuote(Quote quote) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "INSERT INTO itsupportmanager.quotes (description, price, commission_id, user_id) " +
                    "VALUES (?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, quote.getDescription());
            statement.setDouble(2, quote.getPrice());
            statement.setInt(3, quote.getCommissionID());
            statement.setInt(4, ApplicationController.getLoggedUser().getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void modifyQuote(final Quote quote) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "UPDATE itsupportmanager.quotes " +
                    "SET description = ?, price = ?, status = ? " +
                    "WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, quote.getDescription());
            statement.setDouble(2, quote.getPrice());
            statement.setString(3, quote.getStatus().toString());
            statement.setInt(4, ((Quote) ApplicationController.get("quote")).getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void removeQuote(Quote quote) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "DELETE FROM itsupportmanager.quotes q " +
                    "WHERE q.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, quote.getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
