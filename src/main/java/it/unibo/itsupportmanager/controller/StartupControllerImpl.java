package it.unibo.itsupportmanager.controller;

import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.network.NetworkFactory;
import it.unibo.itsupportmanager.tasks.DbConnection;
import it.unibo.itsupportmanager.tasks.UserLogin;
import it.unibo.itsupportmanager.view.StartupView;
import it.unibo.itsupportmanager.view.StartupViewImpl;
import javafx.concurrent.Task;

public final class StartupControllerImpl implements StartupController {

    private final StartupView startupView;
    private NetworkFactory networkFactory;

    public StartupControllerImpl() {
        this.startupView = new StartupViewImpl(this);
    }

    @Override
    public void createFactory(final String host, final String port, final String user, final String password, final String userEmail, final String userPassword) {
        try {
            // Start the Task to get the DB Instance
            Task<NetworkFactory> dbConnection = new DbConnection(host, port, user, password);
            // Bind all callbacks
            this.startupView.getStatusLabel().textProperty().bind(dbConnection.messageProperty());
            dbConnection.setOnRunning(event -> this.startupView.connectingStatus(true));
            dbConnection.setOnFailed(event -> {
                this.startupView.errorDialog(event.getSource().getException().getMessage());
                this.startupView.connectingStatus(false);
            });
            dbConnection.setOnSucceeded(event -> {
                this.startupView.connectingStatus(false);
                this.networkFactory = dbConnection.getValue();
                ApplicationController.setNetworkFactory(this.networkFactory);
                // Continue with User Auth
                loginUser(userEmail, userPassword);
            });
            // Start the thread once everything is bind
            new Thread(dbConnection).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void next() {
        this.startupView.close();
        new MainPageControllerImpl();
    }

    @Override
    public void loginUser(final String email, final String password) {
        // UserLogin takes care of everything. We just need to listen to the events.
        Task<User> userTask = new UserLogin(this.networkFactory, email, password);
        this.startupView.getStatusLabel().textProperty().bind(userTask.messageProperty());
        userTask.setOnRunning(event -> this.startupView.connectingStatus(true));
        userTask.setOnSucceeded(event -> {
            this.startupView.connectingStatus(false);
            ApplicationController.setLoggedUser(userTask.getValue());
            next();
        });
        userTask.setOnFailed(event -> {
            this.startupView.errorDialog(event.getSource().getException().getMessage());
            this.startupView.connectingStatus(false);
        });
        new Thread(userTask).start();
    }

}
