package it.unibo.itsupportmanager.controller;

import it.unibo.itsupportmanager.model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SupportController {

    public static List<Support> getAllSupports() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.assistances a " +
                    "WHERE a.commission_id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ((Commission) ApplicationController.get("commission")).getId());
            ResultSet resultSet = statement.executeQuery();

            List<Support> supportsList = new ArrayList<Support>();
            while (resultSet.next()) {
                supportsList.add(new Support(resultSet.getInt("id"), resultSet.getString("description"), AzioneType.ASSISTENZA, resultSet.getDate("created_on"), resultSet.getString("notes"),
                        SupportStatus.valueOf(resultSet.getString("status")), resultSet.getInt("worked_hours"), resultSet.getInt("commission_id")));
            }
            return supportsList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static Support getSupport(final Support support) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.assistances a " +
                    "WHERE a.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, support.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new Support(resultSet.getInt("id"), resultSet.getString("description"), AzioneType.ASSISTENZA, resultSet.getDate("created_on"), resultSet.getString("notes"),
                    SupportStatus.valueOf(resultSet.getString("status")), resultSet.getInt("worked_hours"), resultSet.getInt("commission_id"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void addSupport(Support support) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "INSERT INTO itsupportmanager.assistances (description, notes, commission_id) " +
                          "VALUES (?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, support.getDescription());
            statement.setString(2, support.getNotes());
            statement.setInt(3, support.getCommissionID());
            statement.execute();
            sSQL = "INSERT INTO itsupportmanager.users_assistances (assistance_id, user_id) " +
                    "VALUES (LAST_INSERT_ID(), ?)";
            statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ApplicationController.getLoggedUser().getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void modifySupport(final Support support) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "UPDATE itsupportmanager.assistances " +
                    "SET description = ?, notes = ?, status = ?, worked_hours = ? " +
                    "WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, support.getDescription());
            statement.setString(2, support.getNotes());
            statement.setString(3, support.getStatus().toString());
            statement.setInt(4, support.getWorkedHours());
            statement.setInt(5, ((Support) ApplicationController.get("support")).getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void removeSupport(Support support) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "DELETE FROM itsupportmanager.assistances a " +
                          "WHERE a.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, support.getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static User getSupportUser(Support support) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT u.* " +
                          "FROM itsupportmanager.users u, itsupportmanager.users_assistances ua " +
                          "WHERE u.id = ua.user_id " +
                          "AND ua.assistance_id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, support.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new User(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("surname"), resultSet.getString("email"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
