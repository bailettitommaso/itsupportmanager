package it.unibo.itsupportmanager.controller;

import it.unibo.itsupportmanager.controller.tabs.TabsController;
import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.network.NetworkFactory;
import javafx.stage.Stage;

import java.util.AbstractMap;
import java.util.HashMap;

public final class ApplicationController {

    private static final AbstractMap<String, Object> map = new HashMap<>();
    private static TabsController tabsController;
    private static User loggedUser;
    private static Stage currentStage;
    private static NetworkFactory networkFactory;

    public static Object get(final String name) {
        return map.get(name);
    }

    public static void set(final String name, final Object object) {
        map.put(name, object);
    }

    public static TabsController getTabsController() {
        return tabsController;
    }

    public static NetworkFactory getNetworkFactory() {
        return networkFactory;
    }

    public static void setNetworkFactory(NetworkFactory networkFactory) {
        ApplicationController.networkFactory = networkFactory;
    }

    public static User getLoggedUser() {
        return loggedUser;
    }

    public static void setLoggedUser(final User user) {
        loggedUser = user;
    }

    public static Stage getCurrentStage() {
        return currentStage;
    }

    public static void setCurrentStage(final Stage stage) {
        currentStage = stage;
    }
}
