package it.unibo.itsupportmanager.controller;

public interface StartupController {

    /**
     * Creates a NetworkFactory.
     *
     * @param host         ip to connect to
     * @param port         port used
     * @param user         username for the database access
     * @param password     password for the db access
     * @param userEmail    email of the user to authenticate
     * @param userPassword password of the user
     */
    void createFactory(String host, String port, String user, String password, String userEmail, String userPassword);

    /**
     * Call the next thing to do.
     */
    void next();

    /**
     * Login the user with the given networkFactory.
     *
     * @param email    email of the user
     * @param password password of the user
     */
    void loginUser(String email, String password);
}
