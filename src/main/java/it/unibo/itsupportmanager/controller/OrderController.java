package it.unibo.itsupportmanager.controller;

import it.unibo.itsupportmanager.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderController {

    public static List<Order> getAllOrders() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.orders o " +
                    "WHERE o.commission_id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ((Commission) ApplicationController.get("commission")).getId());
            ResultSet resultSet = statement.executeQuery();

            List<Order> ordersList = new ArrayList<Order>();
            while (resultSet.next()) {
                ordersList.add(new Order(resultSet.getInt("id"), resultSet.getString("description"), AzioneType.ORDINE, resultSet.getDate("created_on"), OrderStatus.valueOf(resultSet.getString("status")),
                                         resultSet.getString("supplier"), resultSet.getDate("expected_delivery_date"), resultSet.getInt("commission_id"), resultSet.getInt("user_id")));
            }
            return ordersList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static Order getOrder(final Order order) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.orders o " +
                    "WHERE o.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, order.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new Order(resultSet.getInt("id"), resultSet.getString("description"), AzioneType.ORDINE, resultSet.getDate("created_on"), OrderStatus.valueOf(resultSet.getString("status")),
                             resultSet.getString("supplier"), resultSet.getDate("expected_delivery_date"), resultSet.getInt("commission_id"), resultSet.getInt("user_id"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static int addOrder(Order order) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "INSERT INTO itsupportmanager.orders (description, supplier, expected_delivery_date, commission_id, user_id) " +
                    "VALUES (?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, order.getDescription());
            statement.setString(2, order.getSupplier());
            statement.setDate(3, order.getExpectedDelivery());
            statement.setInt(4, order.getCommissionID());
            statement.setInt(5, ApplicationController.getLoggedUser().getId());
            statement.execute();
            sSQL = "SELECT * from itsupportmanager.orders o " +
                    "WHERE o.id = LAST_INSERT_ID()";
            statement = connection.prepareStatement(sSQL);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("id");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public static void addOrderProduct(ProductOrder productOrder, int orderID) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "INSERT INTO itsupportmanager.orders_products (order_id, product_id, quantity, price) " +
                    "VALUES (?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, orderID);
            statement.setInt(2, productOrder.getId());
            statement.setInt(3, productOrder.getQuantity());
            statement.setDouble(4, productOrder.getPrice());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void modifyOrderProduct(ProductOrder productOrder, int orderID) {
        if(productOrder.getQuantity() == 0) {
            try {
                final Connection connection = ApplicationController.getNetworkFactory().getConnection();
                String sSQL = "DELETE FROM itsupportmanager.orders_products op " +
                        "WHERE op.order_id = ? " +
                        "AND op.product_id = ?";
                PreparedStatement statement = connection.prepareStatement(sSQL);
                statement.setInt(1, orderID);
                statement.setInt(2, productOrder.getId());
                statement.execute();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            try {
                final Connection connection = ApplicationController.getNetworkFactory().getConnection();
                String sSQL = "UPDATE itsupportmanager.orders_products " +
                              "SET quantity = ?, price = ? " +
                              "WHERE order_id = ? " +
                              "AND product_id = ?;";
                PreparedStatement statement = connection.prepareStatement(sSQL);
                statement.setInt(1, productOrder.getQuantity());
                statement.setDouble(2, productOrder.getPrice());
                statement.setInt(3, orderID);
                statement.setInt(4, productOrder.getId());
                statement.execute();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static List<ProductOrder> getOrderProducts(int orderID) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.orders_products op, itsupportmanager.products p " +
                    "WHERE op.product_id = p.id " +
                    "AND op.order_id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, orderID);
            ResultSet resultSet = statement.executeQuery();

            List<ProductOrder> productOrderList = new ArrayList<ProductOrder>();
            while (resultSet.next()) {
                productOrderList.add(new ProductOrder(new Product(resultSet.getInt("id"), resultSet.getString("ref"), resultSet.getString("description")),
                        resultSet.getInt("quantity"), resultSet.getDouble("price")));
            }
            return productOrderList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void modifyOrder(final Order order) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "UPDATE itsupportmanager.orders " +
                    "SET description = ?, status = ?, supplier = ?, expected_delivery_date = ? " +
                    "WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, order.getDescription());
            statement.setString(2, order.getStatus().toString());
            statement.setString(3, order.getSupplier());
            statement.setDate(4, order.getExpectedDelivery());
            statement.setInt(5, ((Order) ApplicationController.get("order")).getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void removeOrder(Order order) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "DELETE FROM itsupportmanager.orders o " +
                    "WHERE o.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, order.getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static List<Product> getAllProducts() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.products";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            ResultSet resultSet = statement.executeQuery();

            List<Product> productsList = new ArrayList<Product>();
            while (resultSet.next()) {
                productsList.add(new Product(resultSet.getInt("id"), resultSet.getString("ref"), resultSet.getString("description")));
            }
            return productsList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
