package it.unibo.itsupportmanager.controller.tabs;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommesseController {

    public CommesseController() {
        ApplicationController.getTabsController().setCommesseController(this);
    }

    public static List<Commission> getAllCommissions() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.commissions";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            ResultSet resultSet = statement.executeQuery();

            List<Commission> commissionsList = new ArrayList<Commission>();
            while (resultSet.next()) {
                commissionsList.add(new Commission((Integer) resultSet.getObject(1), (String) resultSet.getObject(2)));
            }
            return commissionsList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static List<Commission> getAllCompanyCommissions() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT cm.* " +
                    "FROM itsupportmanager.commissions cm, itsupportmanager.companies cp " +
                    "WHERE cm.company_id = cp.id " +
                    "AND cp.user_id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ApplicationController.getLoggedUser().getId());
            ResultSet resultSet = statement.executeQuery();

            List<Commission> commissionsList = new ArrayList<Commission>();
            while (resultSet.next()) {
                commissionsList.add(new Commission(resultSet.getInt("id"), resultSet.getString("title"),
                        resultSet.getString("description"), Priority.valueOf(resultSet.getString("priority")),
                        Status.valueOf(resultSet.getString("status")), CommissionSource.valueOf(resultSet.getString("source"))));
            }
            return commissionsList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static Commission getCommission(final Commission commission) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.commissions c " +
                    "WHERE c.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, commission.getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new Commission(resultSet.getInt("id"), resultSet.getString("title"),
            resultSet.getString("description"), Priority.valueOf(resultSet.getString("priority")),
                    Status.valueOf(resultSet.getString("status")), CommissionSource.valueOf(resultSet.getString("source")));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void addCommission(final Commission commission) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "INSERT INTO itsupportmanager.commissions (title, description, priority, status, source, company_id) " +
                    "VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, commission.getTitle());
            statement.setString(2, commission.getDescription());
            statement.setInt(3, Priority.getIntValue(commission.getPriority()));
            statement.setInt(4, Status.getIntValue(commission.getStatus()));
            statement.setInt(5, CommissionSource.getIntValue(commission.getSource()));
            statement.setInt(6, commission.getCompanyID());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void modifyCommission(final Commission commission) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "UPDATE itsupportmanager.commissions " +
                    "SET title = ?, description = ?, priority = ?, status = ?, source = ? " +
                    "WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setString(1, commission.getTitle());
            statement.setString(2, commission.getDescription());
            statement.setInt(3, Priority.getIntValue(commission.getPriority()));
            statement.setInt(4, Status.getIntValue(commission.getStatus()));
            statement.setInt(5, CommissionSource.getIntValue(commission.getSource()));
            statement.setInt(6, ((Commission) ApplicationController.get("commission")).getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void deleteCommission(final Commission commission) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "DELETE FROM itsupportmanager.commissions c " +
                          "WHERE c.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, commission.getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateWorkedHours(final int hours) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "UPDATE itsupportmanager.commissions " +
                    "SET worked_hours = worked_hours + ? " +
                    "WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, hours);
            statement.setInt(2, ((Commission) ApplicationController.get("commission")).getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /*public static int getHoursSum() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT SUM(cm.worked_hours) as sum " +
                    "FROM itsupportmanager.commissions cm, itsupportmanager.companies c " +
                    "WHERE cm.company_id = c.id " +
                    "AND c.user_id = ?";

            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ApplicationController.getLoggedUser().getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return  resultSet.getInt("sum");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }*/

    public static int getHoursSum() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT SUM(a.worked_hours) as sum " +
                    "FROM itsupportmanager.assistances a, itsupportmanager.commissions cm, itsupportmanager.companies c " +
                    "WHERE a.status = 'completed' " +
                    "AND a.commission_id = cm.id " +
                    "AND cm.company_id = c.id " +
                    "AND c.user_id = ?";

            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ApplicationController.getLoggedUser().getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return  resultSet.getInt("sum");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public static User getUser(final int id) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.users u " +
                    "WHERE u.id = ?";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return new User(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("surname"), resultSet.getString("email"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
