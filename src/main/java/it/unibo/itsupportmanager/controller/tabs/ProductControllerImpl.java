package it.unibo.itsupportmanager.controller.tabs;

import it.unibo.itsupportmanager.model.Product;
import it.unibo.itsupportmanager.tasks.products.ProductDelete;
import it.unibo.itsupportmanager.tasks.products.ProductIndex;
import it.unibo.itsupportmanager.view.popup.product.ProductAddControllerImpl;
import it.unibo.itsupportmanager.view.tabs.ProductPanel;
import it.unibo.itsupportmanager.view.tabs.ProductPanelImpl;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;

import java.util.List;

public class ProductControllerImpl implements ProductController {

    private final ProductPanel view;

    public ProductControllerImpl(final Pane superPane) {
        this.view = new ProductPanelImpl(superPane, this);
    }

    @Override
    public void fetchData() {
        final Task<List<Product>> productIndex = new ProductIndex();
        productIndex.setOnSucceeded(event -> this.view.updateProducts(FXCollections.observableList(productIndex.getValue())));
        productIndex.setOnFailed(event -> view.errorDialog(event.getSource().getException().getLocalizedMessage()));
        new Thread(productIndex).start();
    }

    @Override
    public void removeProduct(final Product product) {
        final Task<Void> productDelete = new ProductDelete(product);
        productDelete.setOnSucceeded(event -> {
            this.view.showAlert(Alert.AlertType.INFORMATION, "Prodotto cancellato con successo.");
            this.fetchData();
        });
        productDelete.setOnFailed(event -> this.view.errorDialog("Qualcosa è andato storto: " + event.getSource().getException().getLocalizedMessage()));
        this.view.getRemoveButton().disableProperty().or(productDelete.runningProperty());
        new Thread(productDelete).start();
    }

    @Override
    public void addProduct() {
        new ProductAddControllerImpl(this);
    }
}
