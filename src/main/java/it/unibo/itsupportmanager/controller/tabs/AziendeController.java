package it.unibo.itsupportmanager.controller.tabs;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.Company;
import it.unibo.itsupportmanager.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AziendeController {

    public AziendeController() {
        ApplicationController.getTabsController().setGestioneUtentiController(this);
    }

    public static List<Company> getAllCompanies() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT * " +
                    "FROM itsupportmanager.companies ";
            PreparedStatement statement = connection.prepareStatement(sSQL);
            ResultSet resultSet = statement.executeQuery();

            List<Company> companiesList = new ArrayList<Company>();
            while (resultSet.next()) {
                companiesList.add(new Company(resultSet.getInt("id"), resultSet.getString("ref"), resultSet.getString("name"), resultSet.getInt("user_id")));
            }
            return companiesList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
