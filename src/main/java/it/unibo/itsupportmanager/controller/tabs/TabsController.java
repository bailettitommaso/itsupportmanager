package it.unibo.itsupportmanager.controller.tabs;

public class TabsController {

    private CommesseController commesseController;
    private PacchettiOreController pacchettiOreController;
    private InfoUtenteController infoUtenteController;
    private AziendeController aziendeController;

    public CommesseController getCommesseController() {
        return commesseController;
    }

    public void setCommesseController(CommesseController commesseController) {
        this.commesseController = commesseController;
    }

    public PacchettiOreController getPacchettiOreController() {
        return pacchettiOreController;
    }

    public void setPacchettiOreController(PacchettiOreController pacchettiOreController) {
        this.pacchettiOreController = pacchettiOreController;
    }

    public InfoUtenteController getInfoUtenteController() {
        return infoUtenteController;
    }

    public void setInfoUtenteController(InfoUtenteController infoUtenteController) {
        this.infoUtenteController = infoUtenteController;
    }

    public AziendeController getGestioneUtentiController() {
        return aziendeController;
    }

    public void setGestioneUtentiController(AziendeController aziendeController) {
        this.aziendeController = aziendeController;
    }
}
