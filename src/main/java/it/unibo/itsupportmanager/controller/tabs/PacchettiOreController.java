package it.unibo.itsupportmanager.controller.tabs;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.Pack;
import it.unibo.itsupportmanager.network.NetworkFactory;
import it.unibo.itsupportmanager.view.tabs.PacchettiOreViewController;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PacchettiOreController {

    public PacchettiOreController() {
        ApplicationController.getTabsController().setPacchettiOreController(this);
    }

    public static List<Pack> getAllPack() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();

            String sSQL = "SELECT p.* " +
                    "FROM itsupportmanager.pack p, itsupportmanager.companies c " +
                    "WHERE p.company_id = c.id " +
                    "AND c.user_id = ?";

            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ApplicationController.getLoggedUser().getId());
            ResultSet resultSet = statement.executeQuery();

            List<Pack> packList = new ArrayList<Pack>();
            while (resultSet.next()) {
                packList.add(new Pack(resultSet.getInt("time"), resultSet.getDouble("price")));
            }
            return packList;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void addPack(final int ore, final double prezzo) {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "INSERT INTO itsupportmanager.pack (time, price, company_id) " +
                          "VALUES (?, ?, (SELECT c.id " +
                                         "FROM itsupportmanager.companies c " +
                                         "WHERE c.user_id = ?))";

            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ore);
            statement.setBigDecimal(2, BigDecimal.valueOf(prezzo));
            statement.setInt(3, ApplicationController.getLoggedUser().getId());
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static int getHoursSum() {
        try {
            final Connection connection = ApplicationController.getNetworkFactory().getConnection();
            String sSQL = "SELECT SUM(p.time) AS sum " +
                    "FROM itsupportmanager.pack p, itsupportmanager.companies c " +
                    "WHERE p.company_id = c.id " +
                    "AND c.user_id = ?";

            PreparedStatement statement = connection.prepareStatement(sSQL);
            statement.setInt(1, ApplicationController.getLoggedUser().getId());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return  resultSet.getInt("sum");
        } catch (SQLException throwables) {
        throwables.printStackTrace();
        }
        return 0;
    }

}
