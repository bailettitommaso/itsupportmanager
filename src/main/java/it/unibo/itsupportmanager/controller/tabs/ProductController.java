package it.unibo.itsupportmanager.controller.tabs;

import it.unibo.itsupportmanager.model.Product;

public interface ProductController {

    /**
     * Fetch list of products from the DB.
     */
    void fetchData();

    /**
     * Remove a Product from the DB.
     *
     * @param product product to be removed
     */
    void removeProduct(Product product);

    /**
     * Add a new Product.
     */
    void addProduct();
}
