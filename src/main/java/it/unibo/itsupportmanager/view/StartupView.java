package it.unibo.itsupportmanager.view;

import javafx.scene.control.Label;

public abstract class StartupView extends View {

    /**
     * Sets the submit button to "connecting", disabling it.
     *
     * @param flag true for connecting, false otherwise
     */
    public abstract void connectingStatus(Boolean flag);

    /**
     * Retrieves the Label that handles the text statuses, so it can be used with the Tasks.
     *
     * @return Label initialized in JavaFX
     */
    public abstract Label getStatusLabel();
}
