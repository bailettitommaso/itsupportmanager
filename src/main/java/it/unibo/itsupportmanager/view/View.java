package it.unibo.itsupportmanager.view;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.IOException;

public abstract class View {

    /**
     * The stage that the application needs to run, a new one is generated for each page.
     */
    private final Stage stage;

    /**
     * Default Constructor, a new stage is generated for each view.
     */
    public View() {
        this.stage = new Stage();
    }

    /**
     * Get the Stage.
     *
     * @return A duck.
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Shows an ErrorDialog via JavaFX Thread.
     *
     * @param message message to be displayed
     */
    public final void errorDialog(final String message) {
        new Alert(Alert.AlertType.ERROR, message).showAndWait();
    }

    /**
     * Gracefully close the Stage.
     */
    public final void close() {
        stage.close();
    }

    /**
     * Loads the SystemResource file given, loads itself as the controller, then shows the .fxml loaded.
     *
     * @param viewFile the system resource needed for the view
     */
    public final void startView(final String viewFile) {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(viewFile));
        loader.setController(this);
        try {
            this.getStage().setScene(new Scene(loader.load()));
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, e.getLocalizedMessage()).showAndWait();
            Platform.exit();
        }
        this.getStage().show();
    }
}
