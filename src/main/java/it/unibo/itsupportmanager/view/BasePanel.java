package it.unibo.itsupportmanager.view;

import javafx.scene.control.Alert;

public interface BasePanel {

    /**
     * Print an error message where needed, using the engine of the application.
     *
     * @param message message to be displayed
     */
    void errorDialog(String message);

    /**
     * Show a custom Alert, Type customizable.
     *
     * @param alertType type of the Alert
     * @param message   message to be shown
     */
    void showAlert(Alert.AlertType alertType, String message);

}
