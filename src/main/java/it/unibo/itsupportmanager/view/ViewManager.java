package it.unibo.itsupportmanager.view;

import it.unibo.itsupportmanager.view.tabs.TabsViewController;

public final class ViewManager {

    public static TabsViewController tabsViewController = new TabsViewController();

    public static TabsViewController getTabsViewController() {
        return tabsViewController;
    }
    
}
