package it.unibo.itsupportmanager.view;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.MainPageController;
import it.unibo.itsupportmanager.controller.tabs.ProductControllerImpl;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;


public class MainPageViewImpl extends MainPageView {

    private final MainPageController controller;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab commesseTab;
    @FXML
    private AnchorPane commesseTabPane;
    @FXML
    private AnchorPane commessePane;

    @FXML
    private Tab pacchettiOreTab;
    @FXML
    private AnchorPane pacchettiOreTabPane;
    @FXML
    private AnchorPane pacchettiOrePane;

    @FXML
    private Tab infoUtenteTab;
    @FXML
    private AnchorPane infoUtenteTabPane;
    @FXML
    private AnchorPane infoUtentePane;

    @FXML
    private Tab gestioneUtentiTab;
    @FXML
    private AnchorPane gestioneUtentiTabPane;
    @FXML
    private AnchorPane gestioneUtentiPane;

    @FXML
    private Tab prodottiTab;
    @FXML
    private AnchorPane prodottiTabPane;
    @FXML
    private AnchorPane prodottiPane;

    public MainPageViewImpl(final MainPageController controller) {
        super();
        this.controller = controller;
        FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("layouts/mainPage.fxml"));
        loader.setController(this);
        try {
            this.getStage().setScene(new Scene(loader.load()));
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, e.getLocalizedMessage()).showAndWait();
            Platform.exit();
        }

        // Loading FXMLs in the respective tabs
        //((CommesseViewController)loadFXML(this.commesseTabPane, this.commessePane, "layouts/tabsLayouts/commesse.fxml").getController()).draw();
        loadFXML(this.commesseTabPane, this.commessePane, "layouts/tabsLayouts/commesse.fxml");
        loadFXML(this.pacchettiOreTabPane, this.pacchettiOrePane, "layouts/tabsLayouts/pacchettiOre.fxml");
        loadFXML(this.infoUtenteTabPane, this.infoUtentePane, "layouts/tabsLayouts/infoUtente.fxml");
        loadFXML(this.gestioneUtentiTabPane, this.gestioneUtentiPane, "layouts/tabsLayouts/manageUsers.fxml");
        new ProductControllerImpl(this.prodottiTabPane);

        // Disabling tabs for users without permissions
        switch (ApplicationController.getLoggedUser().getUserType()) {
            case GUEST:
                tabPane.getTabs().remove(gestioneUtentiTab);
                tabPane.getTabs().remove(prodottiTab);
                break;
            case EMPLOYEE:
                tabPane.getTabs().remove(pacchettiOreTab);
                tabPane.getTabs().remove(gestioneUtentiTab);
                break;
            case ADMIN:
                tabPane.getTabs().remove(pacchettiOreTab);
                break;
            default:
                    break;
        }

        this.getStage().show();
    }

    private void loadFXML(final Pane parentPane, Pane childPane, final String FXMLPath) {
        FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(FXMLPath));
        parentPane.getChildren().remove(childPane);
        try {
            childPane = loader.load();
            parentPane.getChildren().add(childPane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
