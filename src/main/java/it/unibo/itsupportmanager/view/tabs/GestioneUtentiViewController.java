package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.model.UserType;
import it.unibo.itsupportmanager.tasks.UserDel;
import it.unibo.itsupportmanager.tasks.UserIndex;
import it.unibo.itsupportmanager.view.PopupDialog;
import it.unibo.itsupportmanager.view.ViewController;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.List;

public final class GestioneUtentiViewController implements ViewController {

    @FXML
    private TableView<User> tableView;
    @FXML
    private TableColumn<User, String> idColumn;
    @FXML
    private TableColumn<User, String> emailColumn;
    @FXML
    private TableColumn<User, UserType> userTypeColumn;
    @FXML
    private TableColumn<User, String> companyName;
    @FXML
    private Button dettagli;
    @FXML
    private Button rimuovi;
    @FXML
    private Button aggiungi;

    public void initialize() {
        ViewManager.getTabsViewController().setGestioneUtentiViewController(this);
        // Column Bindings
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        userTypeColumn.setCellValueFactory(new PropertyValueFactory<>("userType"));
        companyName.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        this.fetchData();
    }

    @FXML
    public void dettagliClicked() {
        ApplicationController.set("user_detail", tableView.getSelectionModel().getSelectedItem());
        PopupDialog.createPopupDialog("layouts/popupLayouts/utente/dettagliUtente.fxml", "Dettagli Utente " + tableView.getSelectionModel().getSelectedItem().getId());
    }

    @FXML
    public void rimuoviClicked() {
        new Alert(Alert.AlertType.CONFIRMATION, "Sei sicuro di voler cancellare l'utente " + tableView.getSelectionModel().getSelectedItem().getEmail() + "?")
                .showAndWait()
                .filter(result -> result == ButtonType.OK)
                .ifPresent(buttonType -> {
                    final Task<Void> userDel = new UserDel(tableView.getSelectionModel().getSelectedItem().getId());
                    rimuovi.disableProperty().bind(userDel.runningProperty());
                    aggiungi.disableProperty().bind(userDel.runningProperty());
                    dettagli.disableProperty().bind(userDel.runningProperty());
                    userDel.setOnSucceeded(event -> {
                        new Alert(Alert.AlertType.INFORMATION, "Utente cancellato con successo.").showAndWait();
                        this.fetchData();
                    });
                    userDel.setOnFailed(event -> new Alert(Alert.AlertType.ERROR, "Qualcosa è andato storto: " + event.getSource().getException().getLocalizedMessage()).showAndWait());
                    new Thread(userDel).start();
                });
    }

    @FXML
    public void aggiungiClicked() {
        PopupDialog.createPopupDialog("layouts/popupLayouts/utente/aggiungiUtente.fxml", "Aggiungi Utente");
    }

    public void fetchData() {
        // Task Setup
        final Task<List<User>> userIndex = new UserIndex(ApplicationController.getNetworkFactory());
        userIndex.setOnSucceeded(event -> {
            tableView.getItems().clear();
            tableView.getItems().addAll(userIndex.getValue());
        });
        userIndex.setOnFailed(event -> new Alert(Alert.AlertType.ERROR, event.getSource().getException().getMessage()).showAndWait());
        // UI Bindings
        tableView.disableProperty().bind(userIndex.runningProperty());
        dettagli.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
        rimuovi.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
        // Thread Launch
        new Thread(userIndex).start();
    }
}
