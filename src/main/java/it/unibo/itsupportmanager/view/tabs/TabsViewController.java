package it.unibo.itsupportmanager.view.tabs;

public class TabsViewController {

    private CommesseViewController commesseViewController;
    private PacchettiOreViewController pacchettiOreViewController;
    private InfoUtenteViewController infoUtenteViewController;
    private GestioneUtentiViewController gestioneUtentiViewController;

    public CommesseViewController getCommesseViewController() {
        return commesseViewController;
    }

    public void setCommesseViewController(CommesseViewController commesseViewController) {
        this.commesseViewController = commesseViewController;
    }

    public PacchettiOreViewController getPacchettiOreViewController() {
        return pacchettiOreViewController;
    }

    public void setPacchettiOreViewController(PacchettiOreViewController pacchettiOreViewController) {
        this.pacchettiOreViewController = pacchettiOreViewController;
    }

    public InfoUtenteViewController getInfoUtenteViewController() {
        return infoUtenteViewController;
    }

    public void setInfoUtenteViewController(InfoUtenteViewController infoUtenteViewController) {
        this.infoUtenteViewController = infoUtenteViewController;
    }

    public GestioneUtentiViewController getGestioneUtentiViewController() {
        return gestioneUtentiViewController;
    }

    public void setGestioneUtentiViewController(GestioneUtentiViewController gestioneUtentiViewController) {
        this.gestioneUtentiViewController = gestioneUtentiViewController;
    }

}
