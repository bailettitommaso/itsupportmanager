package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.controller.tabs.ProductController;
import it.unibo.itsupportmanager.model.Product;
import it.unibo.itsupportmanager.view.PopupDialog;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

import java.io.IOException;

public final class ProductPanelImpl extends ProductPanel {

    private ProductController controller;

    @FXML
    private TableView<Product> tableView;
    @FXML
    private TableColumn<Product, Integer> idColumn;
    @FXML
    private TableColumn<Product, String> refColumn;
    @FXML
    private TableColumn<Product, String> descriptionColumn;
    @FXML
    private Button remove;
    @FXML
    private Button add;

    public ProductPanelImpl(final Pane superPane, final ProductController controller) {
        super(superPane);
        this.controller = controller;
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("layouts/tabsLayouts/products.fxml"));
        loader.setController(this);
        try {
            this.getSuperPane().getChildren().add(loader.load());
        } catch (IOException e) {
            this.errorDialog(e.getLocalizedMessage());
            Platform.exit();
        }
        this.idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.refColumn.setCellValueFactory(new PropertyValueFactory<>("ref"));
        this.descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));

        this.remove.disableProperty().bind(this.tableView.getSelectionModel().selectedItemProperty().isNull());

        this.controller.fetchData();
    }

    @FXML
    public void submitRemove() {
        this.controller.removeProduct(this.tableView.selectionModelProperty().getValue().getSelectedItem());
    }

    @FXML
    public void submitAdd() {
        this.controller.addProduct();
    }

    @Override
    public void updateProducts(ObservableList<Product> products) {
        this.tableView.getItems().clear();
        this.tableView.setItems(products);
    }

    @Override
    public Button getRemoveButton() {
        return remove;
    }
}
