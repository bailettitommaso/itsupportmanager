package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.model.Commission;
import it.unibo.itsupportmanager.model.UserType;
import it.unibo.itsupportmanager.view.PopupDialog;
import it.unibo.itsupportmanager.view.ViewController;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public final class CommesseViewController implements ViewController {

    @FXML
    private TableView<Commission> tableView;
    @FXML
    private TableColumn<Commission, Integer> IDColumn;
    @FXML
    private TableColumn<Commission, String> titleColumn;
    @FXML
    private Button dettagli;
    @FXML
    private Button modifica;
    @FXML
    private Button rimuovi;
    @FXML
    private Button aggiungi;

     public void initialize() {
         ViewManager.getTabsViewController().setCommesseViewController(this);

         IDColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
         titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));


         dettagli.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
         switch (ApplicationController.getLoggedUser().getUserType()) {
             case GUEST:
                 aggiungi.setDisable(true);
                 modifica.setDisable(true);
                 rimuovi.setDisable(true);
                 break;
             case EMPLOYEE:
                 modifica.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
                 rimuovi.setDisable(true);
                 break;
             default:
                 modifica.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
                 rimuovi.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
                 break;
         }
         refreshTable();
    }

    @FXML
    public void dettagliClicked(){
         if (tableView.getSelectionModel().getSelectedItem() != null) {
             ApplicationController.set("commission", tableView.getSelectionModel().getSelectedItem());
             PopupDialog.createPopupDialog("layouts/popupLayouts/commessa/dettagliCommessa.fxml", "Dettagi Commessa " + tableView.getSelectionModel().getSelectedItem().getId());
         }
    }

    @FXML
    public void modificaClicked(){
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            ApplicationController.set("commission", tableView.getSelectionModel().getSelectedItem());
            PopupDialog.createPopupDialog("layouts/popupLayouts/commessa/modificaCommessa.fxml", "Modifica Commessa " + tableView.getSelectionModel().getSelectedItem().getId());
        }
    }

    @FXML
    public void rimuoviClicked(){
         CommesseController.deleteCommission(tableView.getSelectionModel().getSelectedItem());
         refreshTable();
    }

    @FXML
    public void aggiungiClicked() {
         PopupDialog.createPopupDialog("layouts/popupLayouts/commessa/aggiungiCommessa.fxml", "Aggiungi Commessa");
    }

    public void refreshTable() {
        this.tableView.getItems().clear();
        if (ApplicationController.getLoggedUser().getUserType() == UserType.GUEST) {
            this.tableView.getItems().addAll(CommesseController.getAllCompanyCommissions());
        } else {
            this.tableView.getItems().addAll(CommesseController.getAllCommissions());
        }
    }

}
