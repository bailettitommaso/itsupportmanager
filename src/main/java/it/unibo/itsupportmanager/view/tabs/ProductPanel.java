package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.model.Product;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public abstract class ProductPanel extends TabbablePanel {

    public ProductPanel(final Pane superPane) {
        super(superPane);
    }

    /**
     * Update the Table with the Products, this deletes any product that is on the table.
     *
     * @param products to be added
     */
    public abstract void updateProducts(ObservableList<Product> products);

    /**
     * Return the remove button of the view.
     *
     * @return button object
     */
    public abstract Button getRemoveButton();
}
