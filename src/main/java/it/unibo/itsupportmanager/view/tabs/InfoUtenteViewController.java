package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.tasks.UserUpdate;
import it.unibo.itsupportmanager.view.ViewController;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public final class InfoUtenteViewController implements ViewController {

    @FXML
    TextField id;
    @FXML
    TextField name;
    @FXML
    TextField surname;
    @FXML
    TextField email;
    @FXML
    TextField userType;
    @FXML
    Button submit;

    public void initialize() {
        ViewManager.getTabsViewController().setInfoUtenteViewController(this);

        final User user = ApplicationController.getLoggedUser();
        id.setText(String.valueOf(user.getId()));
        name.setText(user.getName().orElse(""));
        surname.setText(user.getSurname().orElse(""));
        email.setText(user.getEmail());
        userType.setText(user.getUserType().name());
    }

    @FXML
    public void submitClicked() {
        final User user = ApplicationController.getLoggedUser();
        user.setName(name.getText());
        user.setSurname(surname.getText());
        final Task<Void> userUpdate = new UserUpdate(user);
        name.disableProperty().bind(userUpdate.runningProperty());
        surname.disableProperty().bind(userUpdate.runningProperty());
        submit.disableProperty().bind(userUpdate.runningProperty());
        userUpdate.setOnSucceeded(event -> {
            ApplicationController.setLoggedUser(user);
            new Alert(Alert.AlertType.INFORMATION, "Utente modificato con successo.").showAndWait();
        });
        userUpdate.setOnFailed(event -> new Alert(Alert.AlertType.ERROR, "Qualcosa è andato storto: " + event.getSource().getException().getLocalizedMessage()));
        new Thread(userUpdate).start();
    }

}
