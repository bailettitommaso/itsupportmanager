package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.view.BasePanelImpl;
import javafx.scene.layout.Pane;

public abstract class TabbablePanel extends BasePanelImpl {

    private final Pane superPane;

    public TabbablePanel(final Pane superPane) {
        this.superPane = superPane;
    }

    public Pane getSuperPane() {
        return superPane;
    }
}
