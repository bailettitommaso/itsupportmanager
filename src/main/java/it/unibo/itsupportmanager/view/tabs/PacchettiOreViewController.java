package it.unibo.itsupportmanager.view.tabs;

import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.controller.tabs.PacchettiOreController;
import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.Pack;
import it.unibo.itsupportmanager.view.PopupDialog;
import it.unibo.itsupportmanager.view.ViewController;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public final class PacchettiOreViewController implements ViewController {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TableView tableView;
    @FXML
    private TableColumn oreColumn;
    @FXML
    private TableColumn prezzoColumn;
    @FXML
    private TextField residue;
    @FXML
    private Button acquista;

    public PacchettiOreViewController() {
    }

    public void initialize() {
        ViewManager.getTabsViewController().setPacchettiOreViewController(this);

        oreColumn.setCellValueFactory(new PropertyValueFactory<Pack,Integer>("time"));
        prezzoColumn.setCellValueFactory(new PropertyValueFactory<Pack, Double>("price"));

        switch (ApplicationController.getLoggedUser().getUserType()) {
            case GUEST:
                break;
            case EMPLOYEE:
            case ADMIN:
                acquista.setDisable(true);
                break;
            default:
                break;
        }
        refresh();

    }

    @FXML
    public void acquistaClicked() {
        PopupDialog.createPopupDialog("layouts/popupLayouts/acquistaOre.fxml", "Acquista Pacchetto Ore");
    }

    public void refresh() {
        residue.setText(String.valueOf(PacchettiOreController.getHoursSum() - CommesseController.getHoursSum()));
        this.tableView.getItems().clear();
        this.tableView.getItems().addAll(PacchettiOreController.getAllPack());
    }

    public AnchorPane getAnchorPane() {
        return this.anchorPane;
    }
}
