package it.unibo.itsupportmanager.view;

import javafx.scene.control.Alert;

public abstract class BasePanelImpl implements BasePanel {

    @Override
    public void errorDialog(final String message) {
        showAlert(Alert.AlertType.ERROR, message);
    }

    @Override
    public void showAlert(Alert.AlertType alertType, String message) {
        new Alert(alertType, message).showAndWait();
    }
}
