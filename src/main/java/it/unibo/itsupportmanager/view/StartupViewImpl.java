package it.unibo.itsupportmanager.view;

import it.unibo.itsupportmanager.controller.StartupController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class StartupViewImpl extends StartupView {

    private final StartupController controller;
    @FXML
    private TextField host;
    @FXML
    private TextField port;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private TextField userEmail;
    @FXML
    private PasswordField userPassword;
    @FXML
    private Button submit;
    @FXML
    private Label statusLabel;

    public StartupViewImpl(final StartupController controller) {
        super();
        this.controller = controller;
        this.startView("layouts/startup.fxml");
    }

    @Override
    public final Label getStatusLabel() {
        return statusLabel;
    }

    public final void connectingStatus(final Boolean flag) {
        submit.setDisable(flag);
        submit.setText(flag ? "Connecting..." : "Connect");
    }

    @FXML
    private void loginSubmit() {
        final String hostText = host.getText().isBlank() ? "localhost" : host.getText();
        final String portText = port.getText().isBlank() ? "3306" : port.getText();
        final String usernameText = username.getText().isBlank() ? "root" : username.getText();

        this.controller.createFactory(hostText, portText, usernameText, password.getText(), userEmail.getText(), userPassword.getText());
    }
}
