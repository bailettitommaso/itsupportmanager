package it.unibo.itsupportmanager.view;

import it.unibo.itsupportmanager.controller.ApplicationController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public final class PopupDialog {

    public static void createPopupDialog(final String path, final String title) {
        Stage stage = new Stage();
        Pane pane = new Pane();
        FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(path));
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(String.valueOf(ClassLoader.getSystemResource("css/main.css")));
        stage.setScene(scene);
        //stage.getIcons().add(new Image(String.valueOf(ClassLoader.getSystemResource("icons/stageIcon.png"))));
        stage.setTitle(title);
        stage.show();
        //ApplicationController.set(title, stage);
        ApplicationController.setCurrentStage(stage);
    }

}
