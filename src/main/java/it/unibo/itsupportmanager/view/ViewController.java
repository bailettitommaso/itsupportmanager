package it.unibo.itsupportmanager.view;

public interface ViewController {

    void initialize();

}
