package it.unibo.itsupportmanager.view.popup.product;

import it.unibo.itsupportmanager.view.BasePanelImpl;
import javafx.beans.property.ReadOnlyBooleanProperty;

public abstract class ProductAddView extends BasePanelImpl {

    /**
     * Close the Dialog, duh.
     */
    public abstract void close();

    public abstract void setApplyDisableProperty(ReadOnlyBooleanProperty bind);

    public abstract void setCancelDisableProperty(ReadOnlyBooleanProperty bind);
}
