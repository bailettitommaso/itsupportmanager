package it.unibo.itsupportmanager.view.popup.product;

import it.unibo.itsupportmanager.controller.tabs.ProductController;
import it.unibo.itsupportmanager.model.Product;
import it.unibo.itsupportmanager.tasks.products.ProductStore;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;

public class ProductAddControllerImpl implements ProductAddController {

    private final ProductAddView view;
    private final ProductController controller;

    public ProductAddControllerImpl(final ProductController controller) {
        this.controller = controller;
        this.view = new ProductAddViewImpl(this);
    }

    @Override
    public void addProduct(final Product product) {
        final Task<Void> productStore = new ProductStore(product);
        productStore.setOnSucceeded(event -> {
            view.showAlert(Alert.AlertType.INFORMATION, "Prodotto aggiunto con successo");
            view.close();
            this.controller.fetchData();
        });
        productStore.setOnFailed(event -> view.errorDialog("Errore nell'inserimento prodotto: " + event.getSource().getException().getLocalizedMessage()));
        this.view.setApplyDisableProperty(productStore.runningProperty());
        this.view.setCancelDisableProperty(productStore.runningProperty());
        new Thread(productStore).start();
    }
}
