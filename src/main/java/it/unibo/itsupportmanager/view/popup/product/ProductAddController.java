package it.unibo.itsupportmanager.view.popup.product;

import it.unibo.itsupportmanager.model.Product;

public interface ProductAddController {

    /**
     * Add a product to the DB.
     *
     * @param product product to add.
     */
    void addProduct(Product product);
}
