package it.unibo.itsupportmanager.view.popup.utente;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import it.unibo.itsupportmanager.model.UserType;
import it.unibo.itsupportmanager.tasks.UserAdd;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.apache.commons.codec.digest.DigestUtils;

public class AggiungiUtenteVC {

    @FXML
    private TextField name;
    @FXML
    private TextField surname;
    @FXML
    private TextField email;
    @FXML
    private ComboBox<UserType> roles;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField confirmPassword;
    @FXML
    private Button submit;

    public void initialize() {
        roles.getItems().addAll(UserType.values());
    }

    @FXML
    public void submitUser() {
        if (!password.getText().equals(confirmPassword.getText())) {
            new Alert(Alert.AlertType.ERROR, "Le password non combaciano.").showAndWait();
            return;
        }
        final User user = new User();
        user.setName(name.getText());
        user.setSurname(surname.getText());
        user.setEmail(email.getText());
        user.setUserType(roles.getSelectionModel().getSelectedItem());
        user.setPassword(DigestUtils.sha256Hex(password.getText()));
        final Task<Void> userAdd = new UserAdd(user);
        submit.disableProperty().bind(userAdd.runningProperty());
        userAdd.setOnSucceeded(event -> {
            new Alert(Alert.AlertType.INFORMATION, "Utente aggiunto con successo").showAndWait();
            ViewManager.getTabsViewController().getGestioneUtentiViewController().fetchData();
            ApplicationController.getCurrentStage().close();
        });
        userAdd.setOnFailed(event -> new Alert(Alert.AlertType.ERROR, "Qualcosa è andato storto: " + event.getSource().getException().getLocalizedMessage()));
        new Thread(userAdd).start();
    }

}
