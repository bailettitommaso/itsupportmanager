package it.unibo.itsupportmanager.view.popup.preventivo;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.QuoteController;
import it.unibo.itsupportmanager.controller.SupportController;
import it.unibo.itsupportmanager.model.AzioneType;
import it.unibo.itsupportmanager.model.Commission;
import it.unibo.itsupportmanager.model.Quote;
import it.unibo.itsupportmanager.model.Support;
import it.unibo.itsupportmanager.view.popup.commessa.DettagliCommessaVC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class AggiungiPreventivoVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TextField prezzo;
    @FXML
    private Button aggiungi;

    public void initialize() {
        aggiungi.disableProperty().bind(descrizione.textProperty().isEmpty());
        aggiungi.disableProperty().bind(prezzo.textProperty().isEmpty());
    }

    @FXML
    public void aggiungiClicked() {
        Quote quote = new Quote(descrizione.getText(), Double.parseDouble(prezzo.getText()), ((Commission) ApplicationController.get("commission")).getId(), AzioneType.ASSISTENZA);
        QuoteController.addQuote(quote);
        ((DettagliCommessaVC) ApplicationController.get("DettagliCommessaVC")).refreshTable();
        ApplicationController.getCurrentStage().close();
    }

}
