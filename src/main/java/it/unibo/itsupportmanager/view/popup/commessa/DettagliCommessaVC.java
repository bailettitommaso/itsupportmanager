package it.unibo.itsupportmanager.view.popup.commessa;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.OrderController;
import it.unibo.itsupportmanager.controller.QuoteController;
import it.unibo.itsupportmanager.controller.SupportController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.model.*;
import it.unibo.itsupportmanager.view.PopupDialog;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static it.unibo.itsupportmanager.model.AzioneType.*;

public class DettagliCommessaVC {

    @FXML
    private TextField titolo;
    @FXML
    private TextArea descrizione;
    @FXML
    private TextField priorita;
    @FXML
    private TextField sorgente;
    @FXML
    private  TextField stato;
    @FXML
    private TableView<Azione> tableView;
    @FXML
    private TableColumn IDColumn;
    @FXML
    private TableColumn tipoColumn;
    @FXML
    private TableColumn descrizioneColumn;
    @FXML
    private TableColumn data;
    @FXML
    private Button dettagli;
    @FXML
    private Button modifica;
    @FXML
    private Button rimuovi;
    @FXML
    private Button aggiungi;

    public void initialize() {
        ApplicationController.set("DettagliCommessaVC", this);

        IDColumn.setCellValueFactory(new PropertyValueFactory<Azione,String>("id"));
        tipoColumn.setCellValueFactory(new PropertyValueFactory<Azione, AzioneType>("type"));
        descrizioneColumn.setCellValueFactory(new PropertyValueFactory<Azione, String>("description"));
        data.setCellValueFactory(new PropertyValueFactory<Azione, Date>("timestamp"));

        Commission commission = CommesseController.getCommission((Commission) ApplicationController.get("commission"));
        titolo.setText(commission.getTitle());
        descrizione.setText(commission.getDescription());
        priorita.setText(commission.getPriority().toString());
        sorgente.setText(commission.getSource().toString());
        stato.setText(commission.getStatus().toString());

        dettagli.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
        switch (ApplicationController.getLoggedUser().getUserType()) {
            case GUEST:
                aggiungi.setDisable(true);
                modifica.setDisable(true);
                rimuovi.setDisable(true);
                break;
            case EMPLOYEE:
                modifica.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
                rimuovi.setDisable(true);
                break;
            default:
                modifica.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
                rimuovi.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());
                break;
        }

        refreshTable();
    }

    @FXML
    public void dettagliClicked() {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            switch(tableView.getSelectionModel().getSelectedItem().getType()) {
                case ASSISTENZA:
                    Support support = (Support) tableView.getSelectionModel().getSelectedItem();
                    ApplicationController.set("support", support);
                    PopupDialog.createPopupDialog("layouts/popupLayouts/assistenza/dettagliAssistenza.fxml", "Dettagli Assistenza " + support.getId());
                    break;
                case ORDINE:
                    Order order = (Order) tableView.getSelectionModel().getSelectedItem();
                    ApplicationController.set("order", order);
                    PopupDialog.createPopupDialog("layouts/popupLayouts/ordine/dettagliOrdine.fxml", "Dettagli Ordine " + order.getId());
                    break;
                case PREVENTIVO:
                    Quote quote = (Quote) tableView.getSelectionModel().getSelectedItem();
                    ApplicationController.set("quote", quote);
                    PopupDialog.createPopupDialog("layouts/popupLayouts/preventivo/dettagliPreventivo.fxml", "Dettagli Preventivo " + quote.getId());
                    break;
            }
        }
    }

    @FXML
    public void modificaClicked() {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            switch(tableView.getSelectionModel().getSelectedItem().getType()) {
                case ASSISTENZA:
                    Support support = (Support) tableView.getSelectionModel().getSelectedItem();
                    ApplicationController.set("support", support);
                    PopupDialog.createPopupDialog("layouts/popupLayouts/assistenza/modificaAssistenza.fxml", "Modifica Assistenza " + support.getId());
                    break;
                case ORDINE:
                    Order order = (Order) tableView.getSelectionModel().getSelectedItem();
                    ApplicationController.set("order", order);
                    PopupDialog.createPopupDialog("layouts/popupLayouts/ordine/modificaOrdine.fxml", "Modifica Ordine " + order.getId());
                    break;
                case PREVENTIVO:
                    Quote quote = (Quote) tableView.getSelectionModel().getSelectedItem();
                    ApplicationController.set("quote", quote);
                    PopupDialog.createPopupDialog("layouts/popupLayouts/preventivo/modificaPreventivo.fxml", "Modifica Preventivo " + quote.getId());
                    break;
            }
        }
    }

    @FXML
    public void rimuoviClicked() {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            switch(tableView.getSelectionModel().getSelectedItem().getType()) {
                case ASSISTENZA:
                    SupportController.removeSupport((Support) tableView.getSelectionModel().getSelectedItem());
                    break;
                case ORDINE:
                    OrderController.removeOrder((Order) tableView.getSelectionModel().getSelectedItem());
                    break;
                case PREVENTIVO:
                    QuoteController.removeQuote((Quote) tableView.getSelectionModel().getSelectedItem());
                    break;
            }
            refreshTable();
        }
    }

    @FXML
    public void aggiungiClicked() {
        PopupDialog.createPopupDialog("layouts/popupLayouts/aggiungiAzione.fxml", "Aggiungi Azione");
    }

    public void refreshTable() {
        this.tableView.getItems().clear();
        this.tableView.getItems().addAll(SupportController.getAllSupports());
        this.tableView.getItems().addAll(OrderController.getAllOrders());
        this.tableView.getItems().addAll(QuoteController.getAllQuotes());
    }

}
