package it.unibo.itsupportmanager.view.popup.ordine;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.OrderController;
import it.unibo.itsupportmanager.model.Order;
import it.unibo.itsupportmanager.model.OrderStatus;
import it.unibo.itsupportmanager.model.ProductOrder;
import it.unibo.itsupportmanager.view.popup.commessa.DettagliCommessaVC;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ModificaOrdineVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TableView<ProductOrder> tableView;
    @FXML
    private TableColumn<ProductOrder, Integer> IDColumn;
    @FXML
    private TableColumn<ProductOrder, String> descrizioneColumn;
    @FXML
    private TableColumn<ProductOrder, Integer> quantitaColumn;
    @FXML
    private TableColumn<ProductOrder, Double> prezzoColumn;
    @FXML
    private TextField fornitore;
    @FXML
    private TextField dataConsegna;
    @FXML
    private ComboBox<OrderStatus> stato;
    @FXML
    private Button modifica;

    Order order = (Order) ApplicationController.get("order");

    public void initialize() {
        stato.getItems().addAll(OrderStatus.values());

        modifica.disableProperty().bind(descrizione.textProperty().isEmpty());
        modifica.disableProperty().bind(fornitore.textProperty().isEmpty());

        IDColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        descrizioneColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        quantitaColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        quantitaColumn.setOnEditCommit(event -> event.getRowValue().setQuantity(event.getNewValue()));
        quantitaColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        prezzoColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        prezzoColumn.setOnEditCommit(event -> event.getRowValue().setPrice(event.getNewValue()));
        prezzoColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableView.getItems().addAll(OrderController.getOrderProducts(order.getId()));

        descrizione.setText(order.getDescription());
        fornitore.setText(order.getSupplier());
        if(order.getExpectedDelivery() != null) {
            dataConsegna.setText(order.getExpectedDelivery().toString());
        }
        stato.getSelectionModel().select(order.getStatus());
    }

    @FXML
    public void modificaClicked() {
        order.setDescription(descrizione.getText());
        order.setSupplier(fornitore.getText());
        if (!dataConsegna.getText().isEmpty()) {
            order.setExpectedDelivery(Date.valueOf(dataConsegna.getText()));
        }
        order.setStatus(stato.getSelectionModel().getSelectedItem());

        OrderController.modifyOrder(order);

        final List<ProductOrder> productOrderList = tableView.getItems()
                .stream()
                .filter(productOrder -> productOrder.getPrice() != 0)
                .collect(Collectors.toList());
        for (ProductOrder productOrder : productOrderList) {
            OrderController.modifyOrderProduct(productOrder, order.getId());
        }

        ((DettagliCommessaVC) ApplicationController.get("DettagliCommessaVC")).refreshTable();
        ApplicationController.getCurrentStage().close();

    }

}
