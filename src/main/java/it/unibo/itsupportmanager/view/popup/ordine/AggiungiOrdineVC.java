package it.unibo.itsupportmanager.view.popup.ordine;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.OrderController;
import it.unibo.itsupportmanager.model.*;
import it.unibo.itsupportmanager.view.popup.commessa.DettagliCommessaVC;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.StringConverter;
import javafx.util.converter.DoubleStringConverter;
import javafx.util.converter.IntegerStringConverter;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

public class AggiungiOrdineVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TableView<ProductOrder> tableView;
    @FXML
    private TableColumn<ProductOrder, Integer> IDColumn;
    @FXML
    private TableColumn<ProductOrder, String> descrizioneColumn;
    @FXML
    private TableColumn<ProductOrder, Integer> quantitaColumn;
    @FXML
    private TableColumn<ProductOrder, Double> prezzoColumn;
    @FXML
    private TextField fornitore;
    @FXML
    private TextField dataConsegna;
    @FXML
    private Button aggiungi;

    public void initialize() {
        IDColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        descrizioneColumn.setCellValueFactory(new PropertyValueFactory<>("description"));

        quantitaColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        quantitaColumn.setOnEditCommit(event -> event.getRowValue().setQuantity(event.getNewValue()));
        quantitaColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        prezzoColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        prezzoColumn.setOnEditCommit(event -> event.getRowValue().setPrice(event.getNewValue()));
        prezzoColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        aggiungi.disableProperty().bind(descrizione.textProperty().isEmpty());
        aggiungi.disableProperty().bind(fornitore.textProperty().isEmpty());

        tableView.getSelectionModel().setCellSelectionEnabled(true);

        final List<ProductOrder> productOrderList = OrderController.getAllProducts().stream()
                .map(product -> new ProductOrder(product, 0, 0))
                .collect(Collectors.toList());
        tableView.setItems(FXCollections.observableList(productOrderList));

    }

    @FXML
    public void aggiungiClicked() {
        final List<ProductOrder> productOrderList = tableView.getItems()
                .stream()
                .filter(productOrder -> productOrder.getQuantity() != 0 && productOrder.getPrice() != 0)
                .collect(Collectors.toList());

        Date date = null;
        if (!dataConsegna.getText().isEmpty()) {
            date = Date.valueOf(dataConsegna.getText());
        }

        int orderID = OrderController.addOrder(new Order(descrizione.getText(), fornitore.getText(), date, ((Commission) ApplicationController.get("commission")).getId(), AzioneType.ORDINE));
        for (ProductOrder productOrder : productOrderList) {
            OrderController.addOrderProduct(productOrder, orderID);
        }

        ((DettagliCommessaVC) ApplicationController.get("DettagliCommessaVC")).refreshTable();
        ApplicationController.getCurrentStage().close();
    }

}