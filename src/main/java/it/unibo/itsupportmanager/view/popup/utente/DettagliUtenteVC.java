package it.unibo.itsupportmanager.view.popup.utente;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class DettagliUtenteVC {

    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private TextField surname;
    @FXML
    private TextField email;
    @FXML
    private TextField role;

    public void initialize() {
        final User user = (User) ApplicationController.get("user_detail");
        id.setText(String.valueOf(user.getId()));
        name.setText(user.getName().orElse(""));
        surname.setText(user.getSurname().orElse(""));
        email.setText(user.getEmail());
        role.setText(user.getUserType().name());
    }

}
