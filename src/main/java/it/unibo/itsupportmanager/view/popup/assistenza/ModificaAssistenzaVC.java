package it.unibo.itsupportmanager.view.popup.assistenza;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.SupportController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.model.Status;
import it.unibo.itsupportmanager.model.Support;
import it.unibo.itsupportmanager.model.SupportStatus;
import it.unibo.itsupportmanager.view.ViewManager;
import it.unibo.itsupportmanager.view.popup.commessa.DettagliCommessaVC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class ModificaAssistenzaVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TextArea note;
    @FXML
    private TextField ore;
    @FXML
    private ComboBox<SupportStatus> stato;
    @FXML
    private Button modifica;

    Support support = (Support) ApplicationController.get("support");

    public void initialize() {
        stato.getItems().addAll(SupportStatus.values());

        modifica.disableProperty().bind(descrizione.textProperty().isEmpty());
        modifica.disableProperty().bind(ore.textProperty().isEmpty());

        descrizione.setText(support.getDescription());
        note.setText(support.getNotes());
        ore.setText(String.valueOf(support.getWorkedHours()));
        stato.getSelectionModel().select(support.getStatus());
    }

    @FXML
    public void modificaClicked() {
        support.setDescription(descrizione.getText());
        support.setNotes(note.getText());
        support.setWorkedHours(Integer.parseInt(ore.getText()));
        support.setStatus(stato.getSelectionModel().getSelectedItem());

        SupportController.modifySupport(support);
        if(stato.getSelectionModel().getSelectedItem() == SupportStatus.completed) {
            //CommesseController.updateWorkedHours(Integer.parseInt(ore.getText()));
        }

        ((DettagliCommessaVC) ApplicationController.get("DettagliCommessaVC")).refreshTable();
        ApplicationController.getCurrentStage().close();

    }

}
