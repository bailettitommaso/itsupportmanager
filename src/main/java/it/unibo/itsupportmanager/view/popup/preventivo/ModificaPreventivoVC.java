package it.unibo.itsupportmanager.view.popup.preventivo;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.QuoteController;
import it.unibo.itsupportmanager.model.Quote;
import it.unibo.itsupportmanager.model.QuoteStatus;
import it.unibo.itsupportmanager.view.popup.commessa.DettagliCommessaVC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.EnumSet;

public class ModificaPreventivoVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TextField prezzo;
    @FXML
    private ComboBox<QuoteStatus> stato;
    @FXML
    private Button modifica;

    Quote quote = (Quote) ApplicationController.get("quote");

    public void initialize() {
        stato.getItems().addAll(QuoteStatus.values());

        modifica.disableProperty().bind(descrizione.textProperty().isEmpty());
        modifica.disableProperty().bind(prezzo.textProperty().isEmpty());

        descrizione.setText(quote.getDescription());
        prezzo.setText(String.valueOf(quote.getPrice()));
        stato.getSelectionModel().select(quote.getStatus());
    }

    @FXML
    public void modificaClicked() {
        quote.setDescription(descrizione.getText());
        quote.setPrice(Double.parseDouble(prezzo.getText()));
        quote.setStatus(stato.getSelectionModel().getSelectedItem());

        QuoteController.modifyQuote(quote);
        ((DettagliCommessaVC) ApplicationController.get("DettagliCommessaVC")).refreshTable();
        ApplicationController.getCurrentStage().close();
    }

}
