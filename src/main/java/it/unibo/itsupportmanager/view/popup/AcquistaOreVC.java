package it.unibo.itsupportmanager.view.popup;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.tabs.PacchettiOreController;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AcquistaOreVC {

    @FXML
    private TextField ore;
    @FXML
    private TextField prezzo;
    @FXML
    private Button acquista;

    public void initialize() {
        prezzo.setText("0");
    }

    @FXML
    public void oreTyped() {
        if (ore.getLength() != 0) {
            prezzo.setText(String.valueOf(Double.parseDouble(ore.getText()) * 10));
        } else {
            prezzo.setText("0");
        }
    }

    @FXML
    public void acquistaClicked() {
        PacchettiOreController.addPack(Integer.parseInt(ore.getText()), Double.parseDouble(prezzo.getText()));
        ViewManager.getTabsViewController().getPacchettiOreViewController().refresh();
        ApplicationController.getCurrentStage().close();
    }

}
