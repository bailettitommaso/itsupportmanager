package it.unibo.itsupportmanager.view.popup;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.view.PopupDialog;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class AggiungiAzioneVC {

    @FXML
    private Button assistenza;
    @FXML
    private Button ordine;
    @FXML
    private Button preventivo;

    public void initialize() {
    }

    @FXML
    public void assistenzaClicked() {
        ApplicationController.getCurrentStage().close();
        PopupDialog.createPopupDialog("layouts/popupLayouts/assistenza/aggiungiAssistenza.fxml", "Aggiungi Assistenza");
    }

    @FXML
    public void ordineClicked() {
        ApplicationController.getCurrentStage().close();
        PopupDialog.createPopupDialog("layouts/popupLayouts/ordine/aggiungiOrdine.fxml", "Aggiungi Ordine");
    }

    @FXML
    public void preventivoClicked() {
        ApplicationController.getCurrentStage().close();
        PopupDialog.createPopupDialog("layouts/popupLayouts/preventivo/aggiungiPreventivo.fxml", "Aggiungi Preventivo");
    }

}
