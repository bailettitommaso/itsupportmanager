package it.unibo.itsupportmanager.view.popup.assistenza;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.SupportController;
import it.unibo.itsupportmanager.model.Support;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class DettagliAssistenzaVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TextArea note;
    @FXML
    private TextField ore;
    @FXML
    private TextField stato;
    @FXML
    private TextField utenteAggiunta;

    public void initialize() {
        Support support = (Support) ApplicationController.get("support");
        descrizione.setText(support.getDescription());
        note.setText(support.getNotes());
        ore.setText(String.valueOf(support.getWorkedHours()));
        stato.setText(support.getStatus().toString());
        utenteAggiunta.setText(SupportController.getSupportUser(support).toString());
    }

}
