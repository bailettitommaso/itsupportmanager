package it.unibo.itsupportmanager.view.popup.ordine;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.OrderController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.model.Order;
import it.unibo.itsupportmanager.model.ProductOrder;
import it.unibo.itsupportmanager.model.Support;
import it.unibo.itsupportmanager.model.User;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.Optional;

public class DettagliOrdineVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TableView<ProductOrder> tableView;
    @FXML
    private TableColumn<ProductOrder, Integer> IDColumn;
    @FXML
    private TableColumn<ProductOrder, String> descrizioneColumn;
    @FXML
    private TableColumn<ProductOrder, Integer> quantitaColumn;
    @FXML
    private TableColumn<ProductOrder, Double> prezzoColumn;
    @FXML
    private TextField fornitore;
    @FXML
    private TextField dataConsegna;
    @FXML
    private TextField stato;
    @FXML
    private TextField utenteAggiunta;

    public void initialize() {
        Order order = (Order) ApplicationController.get("order");
        descrizione.setText(order.getDescription());

        IDColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        descrizioneColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        quantitaColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        prezzoColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableView.getItems().addAll(OrderController.getOrderProducts(order.getId()));

        fornitore.setText(order.getSupplier());
        if(order.getExpectedDelivery() != null) {
            dataConsegna.setText(order.getExpectedDelivery().toString());
        }

        stato.setText(order.getStatus().toString());
        utenteAggiunta.setText(CommesseController.getUser(order.getUserID()).toString());
    }

}
