package it.unibo.itsupportmanager.view.popup.assistenza;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.SupportController;
import it.unibo.itsupportmanager.model.AzioneType;
import it.unibo.itsupportmanager.model.Commission;
import it.unibo.itsupportmanager.model.Support;
import it.unibo.itsupportmanager.view.popup.commessa.DettagliCommessaVC;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

public class AggiungiAssistenzaVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TextArea note;
    @FXML
    private Button aggiungi;

    public void initialize() {
        aggiungi.disableProperty().bind(descrizione.textProperty().isEmpty());
    }

    @FXML
    public void aggiungiClicked() {
        Support support = new Support(descrizione.getText(), note.getText(), ((Commission) ApplicationController.get("commission")).getId(), AzioneType.ASSISTENZA);
        SupportController.addSupport(support);
        ((DettagliCommessaVC) ApplicationController.get("DettagliCommessaVC")).refreshTable();
        ApplicationController.getCurrentStage().close();
    }

}
