package it.unibo.itsupportmanager.view.popup.product;

import it.unibo.itsupportmanager.model.Product;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;

import java.io.IOException;

public final class ProductAddViewImpl extends ProductAddView {

    private final ProductAddController controller;
    private final Dialog<ButtonType> dialog;

    @FXML
    private TextField ref;
    @FXML
    private TextField description;

    public ProductAddViewImpl(final ProductAddController controller) {
        super();
        this.controller = controller;
        this.dialog = new Dialog<>();
        this.dialog.setTitle("Aggiungi Prodotto");
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("layouts/popupLayouts/aggiungiProdotto.fxml"));
        try {
            loader.setController(this);
            this.dialog.getDialogPane().setContent(loader.load());
        } catch (IOException exception) {
            new Alert(Alert.AlertType.ERROR, exception.getLocalizedMessage());
        }
        this.dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        this.dialog.getDialogPane().getButtonTypes().add(ButtonType.APPLY);
        this.dialog.getDialogPane().lookupButton(ButtonType.APPLY).addEventFilter(ActionEvent.ACTION, event -> {
            final Product product = new Product(ref.getText(), description.getText());
            this.controller.addProduct(product);
            event.consume();
        });
        this.ref.requestFocus();
        this.dialog.show();
    }

    @Override
    public void close() {
        this.dialog.close();
    }

    @Override
    public void setApplyDisableProperty(final ReadOnlyBooleanProperty bind) {
        this.dialog.getDialogPane().lookupButton(ButtonType.APPLY).disableProperty().bind(bind);
    }

    @Override
    public void setCancelDisableProperty(final ReadOnlyBooleanProperty bind) {
        this.dialog.getDialogPane().lookupButton(ButtonType.CANCEL).disableProperty().bind(bind);
    }
}
