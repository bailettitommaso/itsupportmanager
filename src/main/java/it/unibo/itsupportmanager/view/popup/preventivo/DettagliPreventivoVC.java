package it.unibo.itsupportmanager.view.popup.preventivo;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.model.Quote;
import it.unibo.itsupportmanager.model.QuoteStatus;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.util.EnumSet;

public class DettagliPreventivoVC {

    @FXML
    private TextArea descrizione;
    @FXML
    private TextField prezzo;
    @FXML
    private TextField stato;
    @FXML
    private TextField utenteAggiunta;

    public void initialize() {
        Quote quote = (Quote) ApplicationController.get("quote");
        descrizione.setText(quote.getDescription());
        prezzo.setText(String.valueOf(quote.getPrice()));
        stato.setText(quote.getStatus().toString());
        utenteAggiunta.setText(CommesseController.getUser(quote.getUserID()).toString());
    }

}
