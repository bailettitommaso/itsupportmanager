package it.unibo.itsupportmanager.view.popup.commessa;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.model.Commission;
import it.unibo.itsupportmanager.model.CommissionSource;
import it.unibo.itsupportmanager.model.Priority;
import it.unibo.itsupportmanager.model.Status;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.EnumSet;

public class ModificaCommessaVC {

    @FXML
    private TextField titolo;
    @FXML
    private TextArea descrizione;
    @FXML
    private ComboBox<Priority> priorita;
    @FXML
    private ComboBox<CommissionSource> sorgente;
    @FXML
    private ComboBox<Status> stato;
    @FXML
    private Button modifica;

    public void initialize() {
        priorita.getItems().addAll(EnumSet.allOf(Priority.class));
        sorgente.getItems().addAll(EnumSet.allOf(CommissionSource.class));
        stato.getItems().addAll(EnumSet.allOf(Status.class));

        modifica.disableProperty().bind(titolo.textProperty().isEmpty());

        Commission commission = CommesseController.getCommission((Commission) ApplicationController.get("commission"));
        titolo.setText(commission.getTitle());
        descrizione.setText(commission.getDescription());
        priorita.getSelectionModel().select(commission.getPriority());
        sorgente.getSelectionModel().select(commission.getSource());
        stato.getSelectionModel().select(commission.getStatus());
    }

    @FXML
    public void modificaClicked() {
            CommesseController.modifyCommission(new Commission(titolo.getText(), descrizione.getText(), priorita.getSelectionModel().getSelectedItem(), stato.getSelectionModel().getSelectedItem(), sorgente.getSelectionModel().getSelectedItem()));
            ViewManager.getTabsViewController().getCommesseViewController().refreshTable();
            ApplicationController.getCurrentStage().close();
    }

}
