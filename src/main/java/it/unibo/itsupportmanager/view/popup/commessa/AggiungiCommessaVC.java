package it.unibo.itsupportmanager.view.popup.commessa;

import it.unibo.itsupportmanager.controller.ApplicationController;
import it.unibo.itsupportmanager.controller.tabs.CommesseController;
import it.unibo.itsupportmanager.controller.tabs.AziendeController;
import it.unibo.itsupportmanager.model.*;
import it.unibo.itsupportmanager.view.ViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.EnumSet;

public class AggiungiCommessaVC {

    @FXML
    private TextField titolo;
    @FXML
    private TextArea descrizione;
    @FXML
    private ComboBox<Priority> priorita;
    @FXML
    private ComboBox<CommissionSource> sorgente;
    @FXML
    private TableView<Company> tableView;
    @FXML
    private TableColumn<Company, Integer> IDColumn;
    @FXML
    private TableColumn<Company, String> nomeColumn;
    @FXML
    private Button aggiungi;

    public void initialize() {
        priorita.getItems().addAll(EnumSet.allOf(Priority.class));
        sorgente.getItems().addAll(EnumSet.allOf(CommissionSource.class));

        IDColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nomeColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        aggiungi.disableProperty().bind(titolo.textProperty().isEmpty().or(
                priorita.getSelectionModel().selectedItemProperty().isNull()).or(
                        sorgente.getSelectionModel().selectedItemProperty().isNull()).or(
                                tableView.getSelectionModel().selectedItemProperty().isNull()));

        //aggiungi.disableProperty().bind(titolo.textProperty().isEmpty());
        //aggiungi.disableProperty().bind(priorita.getSelectionModel().selectedItemProperty().isNull());
        //aggiungi.disableProperty().bind(sorgente.getSelectionModel().selectedItemProperty().isNull());
        //aggiungi.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());

        tableView.getItems().addAll(AziendeController.getAllCompanies());
    }

    @FXML
    public void aggiungiClicked() {
            CommesseController.addCommission(new Commission(titolo.getText(), descrizione.getText(), priorita.getSelectionModel().getSelectedItem(), Status.queue, sorgente.getSelectionModel().getSelectedItem(), tableView.getSelectionModel().getSelectedItem().getId()));
            ViewManager.getTabsViewController().getCommesseViewController().refreshTable();
            ApplicationController.getCurrentStage().close();
    }

}
