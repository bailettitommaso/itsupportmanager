package it.unibo.itsupportmanager.application;

import it.unibo.itsupportmanager.controller.StartupControllerImpl;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Start the application by calling the first Controller.
 */
public final class Startup extends Application {
    @Override
    public void start(final Stage stage) {
        new StartupControllerImpl();
    }
}
