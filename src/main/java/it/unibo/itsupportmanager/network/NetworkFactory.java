package it.unibo.itsupportmanager.network;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class NetworkFactory {

    private final String host;
    private final String username;
    private final String password;

    /**
     * Management Class for the Network-related stuff.
     *
     * @param host host:port of the SQL instance
     * @param username username for the connection
     * @param password password for the connection
     */
    public NetworkFactory(final String host, final String username, final String password) {
        this.host = host;
        this.username = username;
        this.password = password;
    }

    /**
     * Get the Connection getting the .env files.
     *
     * @return Connection to the Database through JDBC.
     * @throws SQLException in case of failure
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://" + this.host + "/itsupportmanager", this.username, this.password);
    }
}
